# Anna Simson @ RWTH, December 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Arctic - Lincoln Sea - CASIMBO-CORE-7_12

id:
  type: string
  value: CASIMBO-CORE-7_12

location:
  type: coordinate
  value: {'N': 82.547890, 'E': -62.381870}
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376

date:
  type: string
  value: 2012-05-10
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376

type_ice:
  type: string
  value: sea ice
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376

form_sea-ice:
  type: string
  value: fast ice
  comment: from Fig. 1 b) and c)
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

development-stage_sea-ice:
  type: string
  value: multi-year ice
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376

polar-region:
  type: string
  value: Arctic
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376

water-body_option:
  type: string
  value: Lincoln Sea
  comment: Sampling was conducted during spring in the first two weeks of May 2010, 2011 and 2012 in the Lincoln Sea.
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

water-body:
  type: string
  value: Lincoln Sea
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376

campaign:
  type: string
  value: CASIMBO
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.080: 264.4,
      0.210: 264.8,
      0.310: 265.1,
      0.410: 265.4,
      0.510: 265.8,
      0.610: 266.1,
      0.710: 266.4,
      0.810: 266.7,
      0.910: 267.0,
      1.010: 267.3,
      1.110: 267.7,
      1.210: 268.0,
      1.310: 268.3,
      1.410: 268.6,
      1.510: 269.0,
      1.610: 269.2,
      1.710: 269.6,
      1.810: 269.9,
      1.910: 270.2,
      2.010: 270.5,
      2.110: 270.8,
      2.210: 271.1,
      2.310: 271.5,
      2.410: 271.8,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376
  comment: accuracy correction
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: Internal ice temperatures were measured on texture cores by drilling holes and inserting a thermometer (Testo 720) immediately after core extraction. Temperatures were measured from surface to bottom at intervals of 0.1 m (cores 1–10, 3–10 and 4–10) and 0.5 m (core 5–10).
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: accuracy ±0.2 °C
  source: Testo_2024
  doi: https://static.testo.com/image/upload/Instruction-manual-and-Software/Instruction-manuals/testo-720-instruction-manual-7808.pdf

calculation-method-temperature_sea-ice:
  type: string
  value: linear interpolation
  comment: linear interpolation of temperature based on surface temperature (measured at 0.1 m depth) and assumed bottom temperature (271.37 K- ~32 ppt)
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.080: 0.0,
      0.210: 0.4,
      0.310: 1.5,
      0.410: 2.3,
      0.510: 3.5,
      0.610: 3.0,
      0.710: 3.5,
      0.810: 4.3,
      0.910: 3.8,
      1.010: 3.9,
      1.110: 4.6,
      1.210: 4.4,
      1.310: 4.0,
      1.410: 4.9,
      1.510: 4.2,
      1.610: 4.8,
      1.710: 5.4,
      1.810: 4.5,
      1.910: 3.1,
      2.010: 3.0,
      2.110: 3.3,
      2.210: 4.4,
      2.310: 4.9,
      2.410: 6.3,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lange_et_al_2015_CASIMBO-CORE-7_12
  doi: https://doi.org/10.1594/PANGAEA.842376
  comment: accuracy correction
  adjusted: 1

measurement-device-salinity_sea-ice:
  type: string
  value: WTW Cond 3300i
  comment: For each section, the ice remaining after cutting was put into plastic containers, melted and analyzed for bulk salinity using a salinometer (WTW 3300i). Bulk salinities are reported in parts per thousand (ppt).
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: Between 0 and 42 ppt accuracy is ± 0.1 at 5 deg C ... 25 deg C and ± 0.2 between 25 deg C and 30 deg C
  source: WTW_2008b
  doi: https://www.labworld.at/wp-content/uploads/2017/09/Bedienungsanleitung-WTW-Cond-3300i-3400i.pdf

thickness_sea-ice:
  type: scalar
  value: 2.46
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Table 3
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

thickness_snow:
  type: scalar
  value: 0.60
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Table 3
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418
