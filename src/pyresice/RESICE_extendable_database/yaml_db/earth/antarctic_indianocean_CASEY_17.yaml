# Anna Simson @ RWTH, September 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - Indian Ocean - CASEY_17

id:
  type: string
  value: CASEY_17

location:
  type: coordinate
  value: {'N': -66.2186, 'E': 110.6506}
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026

date:
  type: string
  value: 2009-11-17
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026

type_ice:
  type: string
  value: sea ice
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026

form_sea-ice:
  type: string
  value: fast ice
  comment: The study site was located in seasonal fast ice
  source: van_der_Merwe_et_al_2011a
  doi: https://doi.org/10.1029/2010jg001628

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: Data presented here focus on undeformed first-year ice and associated Fe distributions.
  source: Lannuzel_et_al_2016a
  doi: https://doi.org/10.12952/journal.elementa.000130

polar-region:
  type: string
  value: Antarctic
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026

water-body:
  type: string
  value: Indian Ocean
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026

campaign:
  type: string
  value: CASEY
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.05: 270.7,
      0.15: 271.0,
      0.25: 270.5,
      0.35: 270.6,
      0.45: 270.5,
      0.55: 270.7,
      0.65: 270.7,
      0.75: 270.5,
      0.85: 270.7,
      0.95: 271.3,
      1.05: 271.5,
      1.15: 271.4,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026
  comment: from C to K and accuracy correction
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: Temperature was measured along a dedicated core every 5 cm using a Testotherm 720 temperature probe (±0.1°C accuracy) by insertion into drilled holes, starting from the basal ice. Where temperature and salinity were measured at different depths within a core, temperature was linearly interpolated to match salinity depth.
  source: van_der_Merwe_et_al_2011a
  doi: https://doi.org/10.1029/2010jg001628

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: accuracy ±0.2 °C
  source: Testo_2024
  doi: https://static.testo.com/image/upload/Instruction-manual-and-Software/Instruction-manuals/testo-720-instruction-manual-7808.pdf

calculation-method-temperature_sea-ice:
  type: string
  value: linear interpolation
  comment: Where temperature and salinity were measured at different depths within a core, temperature was linearly interpolated to match salinity depth.
  source: van_der_Merwe_et_al_2011a
  doi: https://doi.org/10.1029/2010jg001628

salinity_sea-ice:
  type: tabulated
  value: {0.03: 5.5, 0.33: 4.8, 0.73: 4.0, 1.10: 6.4, 1.16: 11.2}
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lannuzel_2016b
  doi: https://doi.org/10.1594/PANGAEA.865026
  comment: accuracy correction
  adjusted: 1

measurement-device-salinity_sea-ice:
  type: string
  value: TPS AQUA-C
  comment: Salinity was measured on melted core sections, melted snow, brines and seawater using a TPS AQUA‐C conductivity meter.
  source: van_der_Merwe_et_al_2011a
  doi: https://doi.org/10.1029/2010jg001628

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.4
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: 0.5% of full scale of selected range and assuming the k = 10 sensor (12.1 is highest measurement) is 0.005*80 = 0.4. 0.5% of full scale of selected range at 25 deg C. k=10 Sensor 0 PSU to 80.0 PSU.
  source: TPS_2024b
  doi: https://cdn.shopify.com/s/files/1/0552/9924/4191/files/Aqua_C_Manual.pdf

thickness_sea-ice:
  type: scalar
  value: 1.15
  dev_pdf: Gauss
  dev_value: 0.024
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: van_der_Merwe_et_al_2011a
  doi: https://doi.org/10.1029/2010jg001628
