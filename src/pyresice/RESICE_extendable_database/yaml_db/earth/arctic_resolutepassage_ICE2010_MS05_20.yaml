# Anna Simson @ RWTH, November 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Arctic - Resolute Passage - ICE2010_MS05_20

id:
  type: string
  value: ICE2010_MS05_20

location:
  type: coordinate
  value: {'N': 74.70800, 'E': -95.24408}
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

date:
  type: string
  value: 2010-05-20
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

type_ice:
  type: string
  value: sea ice
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

form_sea-ice:
  type: string
  value: fast ice
  comment: The sea-ice camp was situated on smooth, landfast, first-year sea ice, above a water column approximately 140 m deep, in the Canadian Arctic Archipelago (CAA; Figure 1b).
  source: Brown_et_al_2015
  doi: https://doi.org/10.1002/2014JC010620

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: The sea-ice camp was situated on smooth, landfast, first-year sea ice, above a water column approximately 140 m deep, in the Canadian Arctic Archipelago (CAA; Figure 1b).
  source: Brown_et_al_2015
  doi: https://doi.org/10.1002/2014JC010620

polar-region:
  type: string
  value: Arctic
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

campaign:
  type: string
  value: Arctic-ICE
  comment: Arctic-ICE (Arctic - Ice-Covered Ecosystem in a Rapidly Changing Environment)
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

water-body:
  type: string
  value: Resolute Passage
  comment: Resolute Passage, Nunavut
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

salinity_sea-ice:
  type: tabulated
  value: {0.260: 4.4, 0.660: 9.0, 1.000: 8.9, 1.330: 5.8}
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798
  comment: depth 1.000 is corrected due to typo in data set and from psu to ppt without changes
  adjusted: 1

measurement-device-salinity_sea-ice_option:
  type: string
  value: WTW Cond 330i
  comment: The salinity (S) of under-ice surface seawater, snow melt, bulk ice, and brine samples was measured with a hand-held conductivity meter (WTW Cond 330i) calibrated to a 0.01 mol L−1 KCl standard solution.
  source: Brown_et_al_2015
  doi: https://doi.org/10.1002/2014JC010620

measurement-device-salinity_sea-ice:
  type: string
  value: WTW Cond 330i
  comment: Conductivity meter, WTW Cond 330i, Nova Analytics
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: Reported accuracy for the hand-held conductivity meter is +/-0.1 over a salinity range of 0-42.
  source: Brown_et_al_2015
  doi: https://doi.org/10.1002/2014JC010620

thickness_snow:
  type: scalar
  value: 0.170
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

freeboard_sea-ice:
  type: scalar
  value: 0.070
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798

thickness_sea-ice:
  type: scalar
  value: 1.430
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Mundy_et_al_2010
  doi: https://doi.org/10.1594/PANGAEA.845798
