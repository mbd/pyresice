import os
# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))
# Change the working directory to the script's directory
os.chdir(script_dir)
parent_dir = os.path.dirname(os.path.dirname(os.path.dirname(script_dir)))

def zenodo_export(df_enriched_onedepth, output_dir = None):
    '''
    Function to export the dataframe to a csv file
    '''
    if output_dir is None:
        output_dir =os.path.join(parent_dir, 'data_output', 'Zenodo')  # Default output directory within the package
    else:
        output_dir = os.path.join(output_dir,'Zenodo')
        
    # Ensure output directory exists
    os.makedirs(output_dir, exist_ok=True)
    # with all values per core
    df_CCBYSA = df_enriched_onedepth.loc[df_enriched_onedepth['date_doi'] == 'https://doi.org/10.1594/PANGAEA.845798']
    df_CCBY = df_enriched_onedepth.loc[df_enriched_onedepth['date_doi'] != 'https://doi.org/10.1594/PANGAEA.845798']
    df_CCBYSA=df_CCBYSA.reset_index(drop=True)
    df_CCBY =df_CCBY.reset_index(drop=True)
    df_CCBY.index.name = 'index'
    df_CCBYSA.index.name = 'index'

    # drop indices

    output_path_PARTA = os.path.join(output_dir, 'RESICE_PartA.csv')
    output_path_PARTB = os.path.join(output_dir, 'RESICE_PartB.csv')
    df_CCBY.to_csv(output_path_PARTA)
    df_CCBYSA.to_csv(output_path_PARTB)

    # with mean values per core
    df_CCBYSA_percore = df_CCBYSA.drop(['depth [m]', 'temperature_sea_ice [K]', 'salinity_sea_ice [ppt]'], axis=1)
    df_CCBY_percore = df_CCBY.drop(['depth [m]', 'temperature_sea_ice [K]', 'salinity_sea_ice [ppt]'], axis=1)
    df_CCBYSA_percore = df_CCBYSA_percore.drop_duplicates()
    df_CCBYSA_percore = df_CCBYSA_percore.reset_index(drop=True)
    df_CCBYSA_percore.index.name = 'index'

    df_CCBY_percore = df_CCBY_percore.drop_duplicates()
    df_CCBY_percore = df_CCBY_percore.reset_index(drop=True)
    df_CCBY_percore.index.name = 'index'
    output_path_PARTA_percore = os.path.join(output_dir, 'RESICE_PartA_cores.csv')
    output_path_PARTB_percore = os.path.join(output_dir, 'RESICE_PartB_cores.csv')
    df_CCBY_percore.to_csv(output_path_PARTA_percore) 
    df_CCBYSA_percore.to_csv(output_path_PARTB_percore)
    return

def webODV_export(df_enriched_onedepth, output_dir = None):

    if output_dir is None:
        output_dir = os.path.join(parent_dir, 'data_output', 'webODV')  # Default output directory within the package
    else:
        output_dir = os.path.join(output_dir,'webODV')
        
    # Ensure output directory exists
    os.makedirs(output_dir, exist_ok=True)

    df_webODV = df_enriched_onedepth
    # Add extra column 'doi' to df_webODV
    df_webODV['doi'] = ''
    # Move "doi" column to the front
    cols = list(df_webODV.columns)
    cols.insert(0, cols.pop(cols.index('doi')))
    df_webODV = df_webODV[cols]
    # Set 'doi' value based on condition
    df_webODV.loc[df_webODV['date_doi'] != 'https://doi.org/10.1594/PANGAEA.845798', 'doi'] = 'https://doi.org/10.5281/zenodo.10866363'
    df_webODV.loc[df_webODV['date_doi'] == 'https://doi.org/10.1594/PANGAEA.845798', 'doi'] = 'https://doi.org/10.5281/zenodo.10866371'
    # Drop columns with _source ending from df_webODV
    columns_to_drop = [col for col in df_webODV.columns if col.endswith('_source')]
    df_webODV = df_webODV.drop(columns=columns_to_drop)
    # Replace commas in strings with -
    df_webODV = df_webODV.replace(',','-', regex=True)
    # Remove " from strings
    df_webODV = df_webODV.replace('"','', regex=True)

    output_path_webODV = os.path.join(output_dir, 'RESICE_webODV.csv')
    df_webODV.to_csv(output_path_webODV)
    return