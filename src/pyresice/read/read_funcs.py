import pandas as pd
import numpy as np
from importlib import resources
from os import walk
import os
import pathlib
from pyresice.RESICE_extendable_database.library.ice_regimes.Regime import Regime
# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))

# Change the working directory to the script's directory
os.chdir(script_dir)

parent_dir = os.path.dirname(os.getcwd())


def load_yaml_files(subpackage_name="pyresice.RESICE_extendable_database.yaml_db.earth"):
    """Get paths to all YAML files within the specified package.

    Parameters
    ----------
    subpackage_name : str, optional
        Name of the subpackage containing the YAML files, by default None.

    Returns
    -------
    YAML_file_list : list
        List of YAML file names.
    data_list : list
        all yaml files stored in regime class
    """
    data_list = []
    YAML_file_list = []
    dirpath = os.path.join(parent_dir, 'RESICE_extendable_database','yaml_db', 'earth') 
    for root, dirs, files in walk(os.path.join(pathlib.Path(__file__).parent.parent), 'RESICE_extendable_database', 'yaml_db', 'earth'):
        if root.endswith(dirpath):
            for file in files:
                if file.endswith(".yaml"):
                    YAML_file_list.append(file)
    for filename in YAML_file_list:
        with resources.path(subpackage_name, filename) as f:
            data_file_path = f
        vars()[filename] = Regime()
        key1 = "{}".format(filename)
        key = locals()[key1]
        key.load_ice_props(data_file_path)
        data_list.append(key)

    return YAML_file_list, data_list


def create_tabular_from_YAML(YAML_file_list, data_list):
    """
    This function takes the list of objects and creates a tabular representation of the YAML files
    """
    # YAML_file_list, data_list = load_yaml_files()
    df = pd.DataFrame()
    for i in range(len(YAML_file_list)):
        try:
            key = data_list[i]
            # check whether more salinity or temperature values available and define array length
            try:
                len_salinity_sea_ice = len(
                    np.array(list(key.ice_props["salinity_sea-ice"]["value"].values()))
                )
            except KeyError:
                len_salinity_sea_ice = 0
            try:
                len_temperature_sea_ice = len(
                    np.array(list(key.ice_props["temperature_sea-ice"]["value"].values()))
                )

            except KeyError:
                len_temperature_sea_ice = 0
            if len_salinity_sea_ice == 0 and len_temperature_sea_ice > 0:
                len_salinity_sea_ice = len_temperature_sea_ice
            else:
                pass
            if (len_temperature_sea_ice == 0) and (len_salinity_sea_ice > 0):
                len_temperature_sea_ice = len_salinity_sea_ice
            else:
                pass
            if len_salinity_sea_ice == 0 and len_temperature_sea_ice == 0:
                continue
            else:
                pass
            len_array = max([len_salinity_sea_ice, len_temperature_sea_ice])
            name_list = []
            id_list = []
            salinity_sea_ice = [None] * len_array
            salinity_sea_ice_doi_list = [None] * len_array
            salinity_sea_ice_source_list = [None] * len_array
            salinity_sea_ice_comment_list = [None] * len_array
            salinity_sea_ice_adjusted_list = [None] * len_array
            depth_salinity_sea_ice = [None] * len_array
            meas_dev_salinity_sea_ice_list = [None] * len_array
            meas_dev_salinity_sea_ice_source_list = [None] * len_array
            meas_dev_salinity_sea_ice_doi_list = [None] * len_array
            meas_dev_salinity_sea_ice_comment_list = [None] * len_array
            meas_dev_acc_salinity_sea_ice_doi_list = [None] * len_array
            meas_dev_acc_salinity_sea_ice_source_list = [None] * len_array
            meas_dev_acc_salinity_sea_ice_comment_list = [None] * len_array
            meas_dev_acc_salinity_sea_ice_list = [None] * len_array
            temperature_sea_ice = [None] * len_array
            temperature_sea_ice_doi_list = [None] * len_array
            temperature_sea_ice_source_list = [None] * len_array
            temperature_sea_ice_comment_list = [None] * len_array
            temperature_sea_ice_adjusted_list = [None] * len_array
            depth_temperature_sea_ice = [None] * len_array
            calc_meth_temperature_sea_ice_list = [None] * len_array
            calc_meth_temperature_sea_ice_doi_list = [None] * len_array
            calc_meth_temperature_sea_ice_source_list = [None] * len_array
            calc_meth_temperature_sea_ice_comment_list = [None] * len_array
            meas_dev_temperature_sea_ice_list = [None] * len_array
            meas_dev_temperature_sea_ice_doi_list = [None] * len_array
            meas_dev_temperature_sea_ice_source_list = [None] * len_array
            meas_dev_temperature_sea_ice_comment_list = [None] * len_array
            meas_dev_acc_temperature_sea_ice_list = [None] * len_array
            meas_dev_acc_temperature_sea_ice_doi_list = [None] * len_array
            meas_dev_acc_temperature_sea_ice_source_list = [None] * len_array
            meas_dev_acc_temperature_sea_ice_comment_list = [None] * len_array
            meas_dev_temperature_air_list = [None] * len_array
            meas_dev_temperature_air_doi_list = [None] * len_array
            meas_dev_temperature_air_source_list = [None] * len_array
            meas_dev_temperature_air_comment_list = [None] * len_array
            meas_dev_acc_temperature_air_list = [None] * len_array
            meas_dev_acc_temperature_air_doi_list = [None] * len_array
            meas_dev_acc_temperature_air_source_list = [None] * len_array
            meas_dev_acc_temperature_air_comment_list = [None] * len_array

            try:
                salinity_sea_ice[:len_salinity_sea_ice] = np.array(
                    list(key.ice_props["salinity_sea-ice"]["value"].values())
                )
                depth_salinity_sea_ice[:len_salinity_sea_ice] = np.array(
                    list(key.ice_props["salinity_sea-ice"]["value"].keys())
                )
                salinity_sea_ice_doi_list = [
                    key.ice_props["salinity_sea-ice"]["doi"]
                ] * len_array
                salinity_sea_ice_source_list = [
                    key.ice_props["salinity_sea-ice"]["source"]
                ] * len_array
                salinity_sea_ice_adjusted_list = [
                    key.ice_props["salinity_sea-ice"]["adjusted"]
                ] * len_array
            except KeyError:
                pass
            try:
                salinity_sea_ice_comment_list = [
                    key.ice_props["salinity_sea-ice"]["comment"]
                ] * len_array
                if key.ice_props["salinity_sea-ice"]["comment"] is np.nan:
                    salinity_sea_ice_comment_list = [None] * len_array
            except KeyError:
                pass

            try:
                meas_dev_salinity_sea_ice_list = [
                    key.ice_props["measurement-device-salinity_sea-ice"]["value"]
                ] * len_array
                meas_dev_salinity_sea_ice_doi_list = [
                    key.ice_props["measurement-device-salinity_sea-ice"]["doi"]
                ] * len_array
                meas_dev_salinity_sea_ice_source_list = [
                    key.ice_props["measurement-device-salinity_sea-ice"]["source"]
                ] * len_array
            except KeyError:
                pass
            try:
                meas_dev_salinity_sea_ice_comment_list = [
                    key.ice_props["measurement-device-salinity_sea-ice"]["comment"]
                ] * len_array
                if (
                    key.ice_props["measurement-device-salinity_sea-ice"]["comment"]
                    is np.nan
                ):
                    meas_dev_salinity_sea_ice_comment_list = [None] * len_array
            except KeyError:
                pass

            try:
                meas_dev_acc_salinity_sea_ice_list = [
                    key.ice_props["measurement-device-accuracy-salinity_sea-ice"]["value"]
                ] * len_array
                meas_dev_acc_salinity_sea_ice_doi_list = [
                    key.ice_props["measurement-device-accuracy-salinity_sea-ice"]["doi"]
                ] * len_array
                meas_dev_acc_salinity_sea_ice_source_list = [
                    key.ice_props["measurement-device-accuracy-salinity_sea-ice"]["source"]
                ] * len_array
            except KeyError:
                pass
            try:
                meas_dev_acc_salinity_sea_ice_comment_list = [
                    key.ice_props["measurement-device-accuracy-salinity_sea-ice"]["comment"]
                ] * len_array
            except KeyError:
                pass

            try:
                temperature_sea_ice[:len_temperature_sea_ice] = np.round(
                    np.array(list(key.ice_props["temperature_sea-ice"]["value"].values())),
                    2,
                )
                depth_temperature_sea_ice[:len_temperature_sea_ice] = np.array(
                    list(key.ice_props["temperature_sea-ice"]["value"].keys())
                )
                temperature_sea_ice_doi_list = [
                    key.ice_props["temperature_sea-ice"]["doi"]
                ] * len_array
                temperature_sea_ice_source_list = [
                    key.ice_props["temperature_sea-ice"]["source"]
                ] * len_array
                temperature_sea_ice_adjusted_list = [
                    key.ice_props["temperature_sea-ice"]["adjusted"]
                ] * len_array
            except KeyError:
                pass
            try:
                temperature_sea_ice_comment_list = [
                    key.ice_props["temperature_sea-ice"]["comment"]
                ] * len_array
                if key.ice_props["temperature_sea-ice"]["comment"] is np.nan:
                    temperature_sea_ice_comment_list = [None] * len_array
            except KeyError:
                temperature_sea_ice_comment_list = [None] * len_array

            try:
                meas_dev_temperature_sea_ice_list = [
                    key.ice_props["measurement-device-temperature_sea-ice"]["value"]
                ] * len_array
                meas_dev_temperature_sea_ice_doi_list = [
                    key.ice_props["measurement-device-temperature_sea-ice"]["doi"]
                ] * len_array
                meas_dev_temperature_sea_ice_source_list = [
                    key.ice_props["measurement-device-temperature_sea-ice"]["source"]
                ] * len_array
            except KeyError:
                pass
            try:
                meas_dev_temperature_sea_ice_comment_list = [
                    key.ice_props["measurement-device-temperature_sea-ice"]["comment"]
                ] * len_array
                if (
                    key.ice_props["measurement-device-temperature_sea-ice"]["comment"]
                    is np.nan
                ):
                    meas_dev_temperature_sea_ice_comment_list = [None] * len_array
            except KeyError:
                pass

            try:
                meas_dev_acc_temperature_sea_ice_list = [
                    key.ice_props["measurement-device-accuracy-temperature_sea-ice"][
                        "value"
                    ]
                ] * len_array
                meas_dev_acc_temperature_sea_ice_doi_list = [
                    key.ice_props["measurement-device-accuracy-temperature_sea-ice"]["doi"]
                ] * len_array
                meas_dev_acc_temperature_sea_ice_source_list = [
                    key.ice_props["measurement-device-accuracy-temperature_sea-ice"][
                        "source"
                    ]
                ] * len_array
            except KeyError:
                pass
            try:
                meas_dev_acc_temperature_sea_ice_comment_list = [
                    key.ice_props["measurement-device-accuracy-temperature_sea-ice"][
                        "comment"
                    ]
                ] * len_array
            except KeyError:
                pass

            try:
                calc_meth_temperature_sea_ice_list = [
                    key.ice_props["calculation-method-temperature_sea-ice"]["value"]
                ] * len_array
                calc_meth_temperature_sea_ice_doi_list = [
                    key.ice_props["calculation-method-temperature_sea-ice"]["doi"]
                ] * len_array
                calc_meth_temperature_sea_ice_source_list = [
                    key.ice_props["calculation-method-temperature_sea-ice"]["source"]
                ] * len_array
            except KeyError:
                pass

            try:
                calc_meth_temperature_sea_ice_comment_list = [
                    key.ice_props["calculation-method-temperature_sea-ice"]["comment"]
                ] * len_array
                if (
                    key.ice_props["calculation-method-temperature_sea-ice"]["comment"]
                    is np.nan
                ):
                    calc_meth_temperature_sea_ice_comment_list = [None] * len_array
            except KeyError:
                pass

            try:
                polar_region = key.ice_props["polar-region"]["value"]
                polar_region_doi = key.ice_props["polar-region"]["doi"]
                polar_region_source = key.ice_props["polar-region"]["source"]
                polar_region_list = [polar_region] * len_array
                polar_region_doi_list = [polar_region_doi] * len_array
                polar_region_source_list = [polar_region_source] * len_array
            except KeyError:
                polar_region_list = [None] * len_array
                polar_region_doi_list = [None] * len_array
                polar_region_source_list = [None] * len_array

            try:
                water_body = key.ice_props["water-body"]["value"]
                water_body_doi = key.ice_props["water-body"]["doi"]
                water_body_source = key.ice_props["water-body"]["source"]
                water_body_list = [water_body] * len_array
                water_body_doi_list = [water_body_doi] * len_array
                water_body_source_list = [water_body_source] * len_array
            except KeyError:
                water_body_list = [None] * len_array
                water_body_doi_list = [None] * len_array
                water_body_source_list = [None] * len_array

            try:
                water_body_comment = key.ice_props["water-body"]["comment"]
                water_body_comment_list = [water_body_comment] * len_array
                if key.ice_props["water-body"]["comment"] is np.nan:
                    water_body_comment_list = [None] * len_array
            except KeyError:
                water_body_comment_list = [None] * len_array

            try:
                form_sea_ice = key.ice_props["form_sea-ice"]["value"]
                form_sea_ice_doi = key.ice_props["form_sea-ice"]["doi"]
                form_sea_ice_source = key.ice_props["form_sea-ice"]["source"]
                form_sea_ice_list = [form_sea_ice] * len_array
                form_sea_ice_source_list = [form_sea_ice_source] * len_array
                form_sea_ice_doi_list = [form_sea_ice_doi] * len_array
            except KeyError:
                form_sea_ice_list = [None] * len_array
                form_sea_ice_doi_list = [None] * len_array
                form_sea_ice_source_list = [None] * len_array

            try:
                form_sea_ice_comment = key.ice_props["form_sea-ice"]["comment"]
                form_sea_ice_comment_list = [form_sea_ice_comment] * len_array
                if key.ice_props["form_sea-ice"]["comment"] is np.nan:
                    form_sea_ice_comment_list = [None] * len_array
            except KeyError:
                form_sea_ice_comment_list = [None] * len_array

            try:
                development_stage_sea_ice = key.ice_props["development-stage_sea-ice"][
                    "value"
                ]
                development_stage_sea_ice_doi = key.ice_props["development-stage_sea-ice"][
                    "doi"
                ]
                development_stage_sea_ice_source = key.ice_props[
                    "development-stage_sea-ice"
                ]["source"]
                development_stage_sea_ice_list = [development_stage_sea_ice] * len_array
                development_stage_sea_ice_source_list = [
                    development_stage_sea_ice_source
                ] * len_array
                development_stage_sea_ice_doi_list = [
                    development_stage_sea_ice_doi
                ] * len_array
            except KeyError:
                development_stage_sea_ice_list = [None] * len_array
                development_stage_sea_ice_doi_list = [None] * len_array
                development_stage_sea_ice_source_list = [None] * len_array

            try:
                development_stage_sea_ice_comment = key.ice_props[
                    "development-stage_sea-ice"
                ]["comment"]
                development_stage_sea_ice_comment_list = [
                    development_stage_sea_ice_comment
                ] * len_array
                if key.ice_props["development-stage_sea-ice"]["comment"] is np.nan:
                    development_stage_sea_ice_comment_list = [None] * len_array
            except KeyError:
                development_stage_sea_ice_comment_list = [None] * len_array

            try:
                thickness_sea_ice = key.ice_props["thickness_sea-ice"]["value"]
                thickness_sea_ice_doi = key.ice_props["thickness_sea-ice"]["doi"]
                thickness_sea_ice_source = key.ice_props["thickness_sea-ice"]["source"]
                thickness_sea_ice_list = [thickness_sea_ice] * len_array
                thickness_sea_ice_source_list = [thickness_sea_ice_source] * len_array
                thickness_sea_ice_doi_list = [thickness_sea_ice_doi] * len_array
            except KeyError:
                thickness_sea_ice_list = [None] * len_array
                thickness_sea_ice_doi_list = [None] * len_array
                thickness_sea_ice_source_list = [None] * len_array

            try:
                thickness_sea_ice_comment = key.ice_props["thickness_sea-ice"]["comment"]
                thickness_sea_ice_comment_list = [thickness_sea_ice_comment] * len_array

                if key.ice_props["thickness_sea-ice"]["comment"] is np.nan:
                    thickness_sea_ice_comment_list = [None] * len_array
            except KeyError:
                thickness_sea_ice_comment_list = [None] * len_array

            try:
                freeboard_sea_ice = key.ice_props["freeboard_sea-ice"]["value"]
                freeboard_sea_ice_doi = key.ice_props["freeboard_sea-ice"]["doi"]
                freeboard_sea_ice_source = key.ice_props["freeboard_sea-ice"]["source"]
                freeboard_sea_ice_list = [freeboard_sea_ice] * len_array
                freeboard_sea_ice_source_list = [freeboard_sea_ice_source] * len_array
                freeboard_sea_ice_doi_list = [freeboard_sea_ice_doi] * len_array
            except KeyError:
                freeboard_sea_ice_list = [None] * len_array
                freeboard_sea_ice_doi_list = [None] * len_array
                freeboard_sea_ice_source_list = [None] * len_array

            try:
                freeboard_sea_ice_comment = key.ice_props["freeboard_sea-ice"]["comment"]
                freeboard_sea_ice_comment_list = [freeboard_sea_ice_comment] * len_array

                if key.ice_props["freeboard_sea-ice"]["comment"] is np.nan:
                    freeboard_sea_ice_comment_list = [None] * len_array
            except KeyError:
                freeboard_sea_ice_comment_list = [None] * len_array

            try:
                thickness_snow = key.ice_props["thickness_snow"]["value"]
                thickness_snow_doi = key.ice_props["thickness_snow"]["doi"]
                thickness_snow_source = key.ice_props["thickness_snow"]["source"]
                thickness_snow_list = [thickness_snow] * len_array
                thickness_snow_doi_list = [thickness_snow_doi] * len_array
                thickness_snow_source_list = [thickness_snow_source] * len_array
            except KeyError:
                thickness_snow_list = [None] * len_array
                thickness_snow_doi_list = [None] * len_array
                thickness_snow_source_list = [None] * len_array

            try:
                thickness_snow_comment = key.ice_props["thickness_snow"]["comment"]
                thickness_snow_comment_list = [thickness_snow_comment] * len_array
                if key.ice_props["thickness_snow"]["comment"] is np.nan:
                    thickness_snow_comment_list = [None] * len_array
            except KeyError:
                thickness_snow_comment_list = [None] * len_array

            try:
                temperature_air = key.ice_props["temperature_air"]["value"]
                temperature_air_doi = key.ice_props["temperature_air"]["doi"]
                temperature_air_source = key.ice_props["temperature_air"]["source"]
                temperature_air_adjusted = key.ice_props["temperature_air"]["adjusted"]
                temperature_air_list = [temperature_air] * len_array
                temperature_air_doi_list = [temperature_air_doi] * len_array
                temperature_air_source_list = [temperature_air_source] * len_array
                temperature_air_adjusted_list = [temperature_air_adjusted] * len_array
            except KeyError:
                temperature_air_list = [None] * len_array
                temperature_air_doi_list = [None] * len_array
                temperature_air_source_list = [None] * len_array
                temperature_air_adjusted_list = [None] * len_array

            try:
                temperature_air_comment = key.ice_props["temperature_air"]["comment"]
                temperature_air_comment_list = [temperature_air_comment] * len_array
                if key.ice_props["temperature_air"]["comment"] is np.nan:
                    temperature_air_comment_list = [None] * len_array
            except KeyError:
                temperature_air_comment_list = [None] * len_array

            try:
                meas_dev_temperature_air_list = [
                    key.ice_props["measurement-device-temperature_air"]["value"]
                ] * len_array
                meas_dev_temperature_air_doi_list = [
                    key.ice_props["measurement-device-temperature_air"]["doi"]
                ] * len_array
                meas_dev_temperature_air_source_list = [
                    key.ice_props["measurement-device-temperature_air"]["source"]
                ] * len_array
            except KeyError:
                pass
            try:
                meas_dev_temperature_air_comment_list = [
                    key.ice_props["measurement-device-temperature_air"]["comment"]
                ] * len_array
                if key.ice_props["measurement-device-temperature_air"]["comment"] is np.nan:
                    meas_dev_temperature_air_comment_list = [None] * len_array
            except KeyError:
                pass

            try:
                meas_dev_acc_temperature_air_list = [
                    key.ice_props["measurement-device-accuracy-temperature_air"]["value"]
                ] * len_array
                meas_dev_acc_temperature_air_doi_list = [
                    key.ice_props["measurement-device-accuracy-temperature_air"]["doi"]
                ] * len_array
                meas_dev_acc_temperature_air_source_list = [
                    key.ice_props["measurement-device-accuracy-temperature_air"]["source"]
                ] * len_array
            except KeyError:
                pass
            try:
                meas_dev_acc_temperature_air_comment_list = [
                    key.ice_props["measurement-device-accuracy-temperature_air"]["comment"]
                ] * len_array
            except KeyError:
                pass
            try:
                date = key.ice_props["date"]["value"]
                date_list = [date] * len_array
                date_doi = key.ice_props["date"]["doi"]
                date_source = key.ice_props["date"]["source"]
                date_comment = key.ice_props["date"]["comment"]
                date_source_list = [date_source] * len_array
                date_comment_list = [date_comment] * len_array
                date_doi_list = [date_doi] * len_array
            except KeyError:
                date_list = [None] * len_array
                date_doi_list = [None] * len_array
                date_source_list = [None] * len_array
                date_comment_list = [None] * len_array

            try:
                campaign = key.ice_props["campaign"]["value"]
                campaign_list = [campaign] * len_array
                campaign_doi = key.ice_props["campaign"]["doi"]
                campaign_source = key.ice_props["campaign"]["source"]
                campaign_comment = key.ice_props["campaign"]["comment"]
                campaign_doi_list = [campaign_doi] * len_array
                campaign_source_list = [campaign_source] * len_array
                campaign_comment_list = [campaign_comment] * len_array
            except KeyError:
                campaign_list = [None] * len_array
                campaign_doi_list = [None] * len_array
                campaign_source_list = [None] * len_array
                campaign_comment_list = [None] * len_array

            try:
                latitude_list = [key.ice_props["location"]["value"]["N"]] * len_array
                longitude_list = [key.ice_props["location"]["value"]["E"]] * len_array

                coordinate_doi = key.ice_props["location"]["doi"]
                coordinate_source = key.ice_props["location"]["source"]
                coordinate_doi_list = [coordinate_doi] * len_array
                coordinate_source_list = [coordinate_source] * len_array
                coordinate_doi_list = [coordinate_doi] * len_array
            except KeyError:
                latitude_list = [None] * len_array
                longitude_list = [None] * len_array
                coordinate_doi_list = [None] * len_array
                coordinate_source_list = [None] * len_array

            try:
                id_list = [key.ice_props["id"]["value"]] * len_array
            except KeyError:
                print("id is missing")
            name_list.extend([key.name] * len_array)
            core_dict = {
                "id": id_list,
                "name": name_list,
                "date": date_list,
                "date_source": date_source_list,
                "date_comment": date_comment_list,
                "date_doi": date_doi_list,
                "latitude": latitude_list,
                "longitude": longitude_list,
                "coordinates_source": coordinate_source_list,
                "coordinates_doi": coordinate_doi_list,
                "polar_region": polar_region_list,
                "polar_region_source": polar_region_source_list,
                "polar_region_doi": polar_region_doi_list,
                "water_body": water_body_list,
                "water_body_comment": water_body_comment_list,
                "water_body_source": water_body_source_list,
                "water_body_doi": water_body_doi_list,
                "campaign": campaign_list,
                "campaign_comment": campaign_comment_list,
                "campaign_source": campaign_source_list,
                "campaign_doi": campaign_doi_list,
                "development_stage_sea_ice": development_stage_sea_ice_list,
                "development_stage_sea_ice_comment": development_stage_sea_ice_comment_list,
                "development_stage_sea_ice_source": development_stage_sea_ice_source_list,
                "development_stage_sea_ice_doi": development_stage_sea_ice_doi_list,
                "form_sea_ice": form_sea_ice_list,
                "form_sea_ice_comment": form_sea_ice_comment_list,
                "form_sea_ice_source": form_sea_ice_source_list,
                "form_sea_ice_doi": form_sea_ice_doi_list,
                "thickness_sea_ice_comment": thickness_sea_ice_comment_list,
                "thickness_sea_ice_source": thickness_sea_ice_source_list,
                "thickness_sea_ice_doi": thickness_sea_ice_doi_list,
                "freeboard_sea_ice_comment": freeboard_sea_ice_comment_list,
                "freeboard_sea_ice_source": freeboard_sea_ice_source_list,
                "freeboard_sea_ice_doi": freeboard_sea_ice_doi_list,
                "thickness_snow_comment": thickness_snow_comment_list,
                "thickness_snow_source": thickness_snow_source_list,
                "thickness_snow_doi": thickness_snow_doi_list,
                "temperature_air_comment": temperature_air_comment_list,
                "temperature_air_source": temperature_air_source_list,
                "temperature_air_doi": temperature_air_doi_list,
                "temperature_air_adjusted": temperature_air_adjusted_list,
                "measurement_device_temperature_air": meas_dev_temperature_air_list,
                "measurement_device_temperature_air_comment": meas_dev_temperature_air_comment_list,
                "measurement_device_temperature_air_source": meas_dev_temperature_air_source_list,
                "measurement_device_temperature_air_doi": meas_dev_temperature_air_doi_list,
                "measurement_device_accuracy_temperature_air [K]": meas_dev_acc_temperature_air_list,
                "measurement_device_accuracy_temperature_air_comment": meas_dev_acc_temperature_air_comment_list,
                "measurement_device_accuracy_temperature_air_source": meas_dev_acc_temperature_air_source_list,
                "measurement_device_accuracy_temperature_air_doi": meas_dev_acc_temperature_air_doi_list,
                "temperature_sea_ice_comment": temperature_sea_ice_comment_list,
                "temperature_sea_ice_source": temperature_sea_ice_source_list,
                "temperature_sea_ice_doi": temperature_sea_ice_doi_list,
                "temperature_sea_ice_adjusted": temperature_sea_ice_adjusted_list,
                "measurement_device_temperature_sea_ice": meas_dev_temperature_sea_ice_list,
                "measurement_device_temperature_sea_ice_comment": meas_dev_temperature_sea_ice_comment_list,
                "measurement_device_temperature_sea_ice_source": meas_dev_temperature_sea_ice_source_list,
                "measurement_device_temperature_sea_ice_doi": meas_dev_temperature_sea_ice_doi_list,
                "measurement_device_temperature_sea_ice": meas_dev_temperature_sea_ice_list,
                "measurement_device_temperature_sea_ice_comment": meas_dev_temperature_sea_ice_comment_list,
                "measurement_device_temperature_sea_ice_source": meas_dev_temperature_sea_ice_source_list,
                "measurement_device_temperature_sea_ice_doi": meas_dev_temperature_sea_ice_doi_list,
                "calculation_method_temperature_sea_ice": calc_meth_temperature_sea_ice_list,
                "calculation_method_temperature_sea_ice_comment": calc_meth_temperature_sea_ice_comment_list,
                "calculation_method_temperature_sea_ice_source": calc_meth_temperature_sea_ice_source_list,
                "calculation_method_temperature_sea_ice_doi": calc_meth_temperature_sea_ice_doi_list,
                "measurement_device_accuracy_temperature_sea_ice [K]": meas_dev_acc_temperature_sea_ice_list,
                "measurement_device_accuracy_temperature_sea_ice_comment": meas_dev_acc_temperature_sea_ice_comment_list,
                "measurement_device_accuracy_temperature_sea_ice_source": meas_dev_acc_temperature_sea_ice_source_list,
                "measurement_device_accuracy_temperature_sea_ice_doi": meas_dev_acc_temperature_sea_ice_doi_list,
                "salinity_sea_ice_comment": salinity_sea_ice_comment_list,
                "salinity_sea_ice_source": salinity_sea_ice_source_list,
                "salinity_sea_ice_doi": salinity_sea_ice_doi_list,
                "salinity_sea_ice_adjusted": salinity_sea_ice_adjusted_list,
                "measurement_device_salinity_sea_ice": meas_dev_salinity_sea_ice_list,
                "measurement_device_salinity_sea_ice_comment": meas_dev_salinity_sea_ice_comment_list,
                "measurement_device_salinity_sea_ice_source": meas_dev_salinity_sea_ice_source_list,
                "measurement_device_salinity_sea_ice_doi": meas_dev_salinity_sea_ice_doi_list,
                "measurement_device_accuracy_salinity_sea_ice [ppt]": meas_dev_acc_salinity_sea_ice_list,
                "measurement_device_accuracy_salinity_sea_ice_comment": meas_dev_acc_salinity_sea_ice_comment_list,
                "measurement_device_accuracy_salinity_sea_ice_source": meas_dev_acc_salinity_sea_ice_source_list,
                "measurement_device_accuracy_salinity_sea_ice_doi": meas_dev_acc_salinity_sea_ice_doi_list,
                "thickness_sea_ice [m]": thickness_sea_ice_list,
                "freeboard_sea_ice [m]": freeboard_sea_ice_list,
                "thickness_snow [m]": thickness_snow_list,
                "temperature_air [K]": temperature_air_list,
                "depth_temperature_sea_ice [m]": depth_temperature_sea_ice,
                "temperature_sea_ice [K]": temperature_sea_ice,
                "depth_salinity_sea_ice [m]": depth_salinity_sea_ice,
                "salinity_sea_ice [ppt]": salinity_sea_ice,
            }
            core_df = pd.DataFrame(core_dict).dropna(how="all", axis=1)
            df = pd.concat([df, core_df], ignore_index=True)
        except:
            print("Error in YAML file: {}".format(YAML_file_list[i]))

    return df
