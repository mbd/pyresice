# Anna Simson @RWTH, December 2022
#
#  - type             # [ scalar, array, tabulated, expression ]
#  - value            # float
#  - dev_pdf           # Gauss or other parametrized or tabulated PDF
#  - dev_value          # hyperparameters of PDF or array
#  - unit_str           # standard string to inidate unit
#  - unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable           # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str      # standard string to indicate variable_unit
#  - source [ string ]      # data source
#  - meta_sys [ string ]     # meta data from systematic databases, e.g. NASA database
#  - meta_free [ string]     # free text meta data

name: Antarctica - Weddell Sea - PS117-21_salinity

id:
  type: string
  value: PS117-21_salinity

location:
  type: coordinate
  value: {'N': -70.2967, 'E': -9.999017}
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

date:
  type: string
  value: 2019-01-10
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

type_ice:
  type: string
  value: sea ice
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

form_sea-ice:
  type: string
  value: pack ice
  comment: Station lays in Eastern Weddell Sea. Tab. 7.1 rather shows 7 coordinates in eastern weddell sea and one in north western weddell sea. A total of 8 sea ice stations were sampled during PS117. Five ice stations were conducted in the Eastern Weddell Sea pack-ice zone and three stations in Western/North-Western Weddell Sea marginal ice zone (Tab. 7.1and Fig. 7.1).
  source: Boebel_2019
  doi: https://doi.org/10.2312/BzPM_0732_2019

polar-region:
  type: string
  value: Antarctic
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

water-body:
  type: string
  value: Weddell Sea
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

campaign:
  type: string
  value: PS117
  comment: RV Polarstern PS117 voyage
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

salinity_sea-ice:
  type: tabulated
  value: {0.125: 3.50, 0.375: 3.60, 0.63: 2.90, 0.81: 3.70}
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6
  adjusted: 0

thickness_snow:
  type: string
  value: 0.04
  dev_pdf: Gauss
  dev_value: 0.01
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

freeboard_sea-ice:
  type: string
  value: 0.04
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6

thickness_sea-ice:
  type: string
  value: 0.86
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Meiners_2019
  doi: https://doi.org/10.26179/5D9AC6A8CECC6
