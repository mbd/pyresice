# Anna Simson @ RWTH, January 2022
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Arctic - Central Arctic Ocean - CHINARE_2010_S5

id:
  type: string
  value: CHINARE_2010_S5

location:
  type: coordinate
  value: {'N': 88.066667, 'E': -177.033333}
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

date:
  type: string
  value: 2010-08-19
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

type_ice:
  type: string
  value: sea ice
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

form_sea-ice:
  type: string
  value: pack ice
  comment: derived from SIN_WMO_2014 expecting high sea ice concentration. Vast ice floes with diameters of several kilometers.
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

development-stage_sea-ice:
  type: string
  value: first-year ice
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

polar-region:
  type: string
  value: Arctic
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

water-body_option:
  type: string
  value: Pacific
  comment: Pacific sector of the Arctic
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

water-body:
  type: string
  value: Central Arctic Ocean
  comment: from Fig. 1
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

campaign:
  type: string
  value: CHINARE 2010
  comment: Chinese National Arctic Research Expedition (CHINARE) programs in 2008/10/12/14/16/18
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

temperature_air:
  type: tabulated
  value: 272.95
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867
  comment: from C to K
  adjusted: 1

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.10: 272.9,
      0.15: 272.8,
      0.20: 272.8,
      0.30: 272.8,
      0.40: 272.6,
      0.50: 272.5,
      0.60: 272.4,
      0.70: 272.3,
      0.80: 272.2,
      0.85: 272.2,
      0.95: 272.1,
      1.00: 272.1,
      1.05: 272.0,
      1.10: 271.9,
      1.15: 271.8,
      1.20: 271.8,
      1.25: 271.8,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867
  comment: from C to K
  adjusted: 1

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: The sea ice temperature was measured at each site immediately after lifting the ice core using a fast-response probe thermometer (accuracy ±0.2 °C, Testo, Germany). The probe was inserted in 2-mm diameter holes drilled to the core center at 5- to 10-cm intervals along the core length.
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

measurement-device-temperature_sea-ice:
  type: string
  value: Testo
  comment: The sea ice temperature was measured at each site immediately after lifting the ice core using a fast-response probe thermometer (accuracy ±0.2 °C, Testo, Germany). The probe was inserted in 2-mm diameter holes drilled to the core center at 5- to 10-cm intervals along the core length.
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.05: 0.0,
      0.10: 0.1,
      0.20: 0.0,
      0.25: 0.2,
      0.30: 0.8,
      0.40: 1.3,
      0.50: 1.7,
      0.60: 2.3,
      0.70: 2.5,
      0.80: 2.9,
      1.00: 2.9,
      1.10: 3.1,
      1.15: 3.0,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867
  comment: from psu without changed
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: salinometer
  comment: The salinity samples were melted aboard the ship, and a salinometer with an accuracy of 0.1 practical salinity unit (psu) was used to measure the salinity at a room temperature.
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

thickness_snow:
  type: scalar
  value: 0.10
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

freeboard_sea-ice:
  type: scalar
  value: 0.11
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

thickness_sea-ice:
  type: scalar
  value: 1.217
  dev_pdf: Gauss
  dev_value: 0.058
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867
