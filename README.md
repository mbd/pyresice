# pyresice

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11198658.svg)](https://doi.org/10.5281/zenodo.11198658)

## Description
Pyresice contains the software used to create the Reusability-targeted Enriched Sea Ice Core Database (RESICE) and can be used to extend or reproduce the database.
## Installation

1. Clone the repository
```
git clone https://git.rwth-aachen.de/mbd/pyresice.git
```
2. Download Conda: If you haven not already installed [Conda](https://conda.io/projects/conda/en/latest/index.html), you can download it and follow the installation instructions for your operating system.

3. Create a virtual environment.
```
conda env create --file environment.yml
```

4. Once the installation is complete, activate the environment
```
conda activate pyresice
```

5. Install Poetry using the [official installer](https://python-poetry.org/docs/#installing-with-the-official-installer). 

6. Navigate to the cloned repository: Change your current directory to the directory where you have cloned the repository containing the pyresice package.

7. Install the dependencies of pyresice managed by Poetry
```
poetry install
```

## Usage
The [test_pyresice.ipynb](https://git.rwth-aachen.de/mbd/pyresice/-/blob/main/tests/test_pyresice.ipynb) demonstrates the functionality of pyresice.

## Contributing
Users are encouraged to extend RESICE by adding more sea ice cores or more features through the RESICE extendable database. If you want to add data of a sea ice core not yet included in RESICE please create a new YAML following the structure and naming scheme described in the [readme.md](https://git.rwth-aachen.de/mbd/pyresice/-/blob/main/src/pyresice/RESICE_extendable_database/yaml_db/readme.md) provided in the YAML database. If you want to add data to a core already included in the database please make the changes directly in the respective YAML file. After creating a new YAML file or performing changes in an existing one please start a pull request as described in the CONTRIBUTING.md file. Once notified, we will check the consistency of the YAML file and then merge the changes with RESICE. Furthermore, you could also add a function to the automatic enrichment, for instance, to add classes for the melting stages based on the WMO Sea Ice Nomenclature. In this case plases also start a pull request.
Check out the contributing guidelines. Please note that this project is released with a Code of Conduct. By contributing to this project, you agree to abide by its terms.

## License
Pyresice was created by Anna Simson at the Chair of Methods for Model-based Development in Computational Engineering (RWTH Aachen University, Germany). 
`pyresice` is licensed with GPLv3 License. 

## Credits
`pyresice` was created with [`cookiecutter`](https://cookiecutter.readthedocs.io/en/latest/) and the `py-pkgs-cookiecutter` [template](https://github.com/py-pkgs/py-pkgs-cookiecutter).
