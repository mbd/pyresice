# Anna Simson @ RWTH, November 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Arctic - Beaufort Sea - CFL07_D6_44

id:
  type: string
  value: CFL07_D6_44

location:
  type: coordinate
  value: {'N': 71.335, 'E': -125.199}
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647

date:
  type: string
  value: 2007-12-09
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647

campaign_option:
  type: string
  value: CFL
  comment: 2007/2008 Circumpolar Flaw Lead (CFL) System Study
  source: Pucko_et_al_2011b
  doi: https://doi.org/10.1029/2010JC006614

campaign:
  type: string
  value: CFL
  comment: CCGSA_4-10_CFL08 (Circumpolar Flaw Lead Leg 4-10a)
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647

type_ice:
  type: string
  value: sea ice
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647

development-stage_sea-ice:
  type: string
  value: white-ice
  source: Isleifson_et_al_2010a
  doi: https://doi.org/10.1594/PANGAEA.811539

polar-region:
  type: string
  value: Arctic
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647

water-body:
  type: string
  value: Beaufort Sea
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.05: 262.3,
      0.10: 261.4,
      0.15: 264.1,
      0.20: 264.2,
      0.25: 264.9,
      0.30: 265.1,
      0.35: 265.6,
      0.40: 266.1,
      0.45: 267.3,
      0.50: 268.2,
      0.55: 269.1,
      0.60: 269.9,
      0.65: 270.9,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647
  comment: from C to K
  adjusted: 1

measurement-device-temperature_sea-ice_option:
  type: string
  value: Control Company 4000
  comment: Control Company 4000 with Accuracy ± 0.05°C between 0 to 100°C
  source: Control_Company_2016
  doi: https://www.novatech-usa.com/pdf/Control%20Company%204000%20Instruction%20Manual.pdf

measurement-device-temperature_sea-ice:
  type: string
  value: Control Company 4000
  comment: Sea ice temperature profiles were measured immediately after extracting the ice core by placing a Traceable digital thermometer (model 4000, Control Company, Friendswood, USA (±0.05°C)), in 5 mm diameter holes drilled at 5–10 cm intervals starting from the bottom of the ice core.
  source: Pucko_et_al_2011b
  doi: https://doi.org/10.1029/2010JC006614

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.05
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: model 4000, Control Company, Friendswood, USA (±0.05°C)
  source: Pucko_et_al_2011b
  doi: https://doi.org/10.1029/2010JC006614

temperature_air:
  type: scalar
  value: 251.4
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Isleifson_et_al_2010a
  doi: https://doi.org/10.1594/PANGAEA.811539
  comment: from C to K
  adjusted: 1

measurement-device-temperature_air:
  type: string
  value: Control Company
  comment: Traceable Digital Thermometer, Control Company, accuracy of ±0.05 degree C) approximately 1 m above the ice surface
  source: Isleifson_et_al_2010b
  doi: https://doi.org/10.1109/TGRS.2010.2043954

measurement-device-accuracy-temperature_air:
  type: scalar
  value: 0.05
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: Traceable Digital Thermometer, Control Company, accuracy of ±0.05 degree C) approximately 1 m above the ice surface
  source: Isleifson_et_al_2010b
  doi: https://doi.org/10.1109/TGRS.2010.2043954

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.05: 7.6,
      0.10: 6.4,
      0.15: 5.9,
      0.20: 5.8,
      0.25: 5.4,
      0.30: 5.3,
      0.35: 5.0,
      0.40: 4.8,
      0.45: 4.5,
      0.50: 4.1,
      0.55: 3.7,
      0.60: 3.6,
      0.65: 4.8,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: Hach sensION5
  comment: Salinity of sea ice, snow, and brine samples was calculated from conductivity and temperature using a HACH SENSION5 portable conductivity meter (Hach, Loveland, USA (±0.01)).
  source: Pucko_et_al_2011b
  doi: https://doi.org/10.1029/2010JC006614

measurement-device-accuracy-salinity_sea-ice_option:
  type: scalar
  value: 0.01
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: Unit for accuracy is missing. Salinity of sea ice, snow, and brine samples was calculated from conductivity and temperature using a HACH SENSION5 portable conductivity meter (Hach, Loveland, USA (±0.01)).
  source: Pucko_et_al_2011b
  doi: https://doi.org/10.1029/2010JC006614

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: Salinity Accuracy ±0.1 ppt (-2 to 35°C)
  source: Hach_2000
  doi: https://www.fondriest.com/pdf/hach_sension5_manual.pdf

thickness_sea-ice:
  type: scalar
  value: 0.69
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Pucko_et_al_2011a
  doi: https://doi.org/10.1594/PANGAEA.818647
