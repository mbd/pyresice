# yaml-db

This document explains the structure of the yaml files in the yaml-db

## Files

Each yaml-file is composed of meta(data) entries that we call fields.

Each file should contain the following fields:

- "name" that gives the name similar to the filename (e.g., Arctic - Beaufort Sea - CFL07_405_23),
- "location" that gives the coordinates
- and other fields that depend on the available measurement (meta-)data.

All fields except from name are composed of subfields. Subfields specify the (meta-)data. The subfields type, value, unit and unit_str are required. The subfield source should be included, and other subfields can be omitted. Fields have the following structure:

```
field:
  type: STR                         # String out of [ scalar, tabulated, coordinate, string ].
  value: VAL                        # A value of type float, integer, string or dictionary.
  dev_pdf: STR                      # Gauss or other parametrized or tabulated PDF.
  dev_value: VAL                    # Hyperparameters of PDF or array with same type as value.
  unit_str: STR                     # Standard string to inidate unit.
  unit: [ 0 0 0 0 0 0 0 ]           # An array of the form [ kg m s K A mol cd ] that gives the unit as the exponent of
                                    # The SI basis units, e.g., m/s^2 is [ 0 1 -2 0 0 0 0 ].
  variable: STR                     # Function argument (e.g., temperature) (must be used if type is tabulated or
                                    # expression).
  variable_unit: [ 0 0 0 0 0 0 0 ]  # See above (must be used if type is tabulated or expression).
  variable_unit_str: STR            # Standard string to indicate variable_unit.
  source: STR                       # String with key (e.g. Schmit_et_al_2006a) of data source.
  doi: STR                          # doi or url of the data source
  comment: STR                      # specific details on how the field was filled can be excerpt, figure or table from source or comment on inconsistency
  adjusted: VAL                     # Adjustment of measurement value compared to original sources (1.0) or equivalent (0.0)
```

## Fields

The names of the different fields should have the structure (PROPERTY)\_(OBJECT)\_(ATTRIBUTE) or (PROPERTY)\_(OBJECT). The name consists only of (PROPERTY) if it is independent of any object (here material). If two different sources provide the content of a field, the second source is assigned the **option** Attribute. For property names consisting of two or more words, the words are linked by hyphens. Examples are development-stage and polar-region.
The following list gives all properties, objects and attributes known in the RE-SICE in the Ice Data Hub, yet. A description of all field names is provided in _field_description.md_.

| Property                                | Object  | Attribute |
| --------------------------------------- | ------- | --------- |
| calculation-method-temperature          | air     | option    |
| campaign                                | sea-ice |           |
| date                                    | snow    |           |
| development-stage                       |         |           |
| form                                    |         |           |
| freeboard                               |         |           |
| location                                |         |           |
| measurement-device-accuracy-salinity    |         |           |
| measurement-device-accuracy-temperature |         |           |
| measurement-device-salinity             |         |           |
| measurement-device-temperature          |         |           |
| polar-region                            |         |           |
| salinity                                |         |           |
| temperature                             |         |           |
| thickness                               |         |           |
| type                                    |         |           |
| water-body                              |         |           |

## sources.md

The file sources.md contains all references to the subfield source entries the yaml files.
