# Anna Simson @ RWTH, January 2022
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - Indian Ocean - SIPEX2-04

id:
  type: string
  value: SIPEX2-04

location:
  type: coordinate
  value: {'N': -65.078000, 'E': 121.672000}
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035

campaign:
  type: string
  value: SIPEX2
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035

date:
  type: string
  value: 2012-10-06
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035

type_ice:
  type: string
  value: sea ice
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035

form_sea-ice:
  type: string
  value: pack ice
  comment: Our study quantified the spatial and temporal distribution of Fe and ancillary biogeochemical parameters at six stations visited during an interdisciplinary Australian Antarctic marine science voyage (SIPEX-2) within the East Antarctic first-year pack ice zone during September–October 2012.
  source: Lannuzel_et_al_2016b
  doi: https://doi.org/10.1016/j.dsr2.2014.12.003

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: Our study quantified the spatial and temporal distribution of Fe and ancillary biogeochemical parameters at six stations visited during an interdisciplinary Australian Antarctic marine science voyage (SIPEX-2) within the East Antarctic first-year pack ice zone during September–October 2012.
  source: Lannuzel_et_al_2016b
  doi: https://doi.org/10.1016/j.dsr2.2014.12.003

polar-region:
  type: string
  value: Antarctic
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035

water-body:
  type: string
  value: Indian Ocean
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.020: 270.6,
      0.070: 269.5,
      0.120: 269.0,
      0.170: 269.0,
      0.230: 269.1,
      0.295: 269.6,
      0.345: 270.0,
      0.395: 270.0,
      0.445: 270.3,
      0.495: 270.7,
      0.545: 270.7,
      0.595: 270.8,
      0.645: 270.9,
      0.695: 271.0,
      0.745: 271.2,
      0.795: 271.0,
      0.845: 271.2,
      0.895: 271.2,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035
  comment: from C to K and accuracy correction
  adjusted: 1

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.035: 12.6,
      0.105: 7.1,
      0.175: 9.1,
      0.245: 7.3,
      0.315: 6.0,
      0.385: 4.8,
      0.455: 4.7,
      0.525: 4.6,
      0.595: 4.0,
      0.665: 4.2,
      0.735: 4.5,
      0.805: 4.9,
      0.885: 7.1,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lannuzel_2016e
  doi: https://doi.org/10.1594/PANGAEA.865035
  comment: accuracy correction
  adjusted: 1

thickness_sea-ice:
  type: scalar
  value: 0.95
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Fig. 2
  source: Lannuzel_et_al_2016b
  doi: https://doi.org/10.1016/j.dsr2.2014.12.003

thickness_snow:
  type: scalar
  value: 0.3
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Fig. 2
  source: Lannuzel_et_al_2016b
  doi: https://doi.org/10.1016/j.dsr2.2014.12.003

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: probably same probe as used in CASEY Lannuzel.2016_SIPEX2_paper references van_der_Merwe_et_al_2011a. A temperature probe (Testo, ± 0.1 °C accuracy) was inserted in holes freshly drilled along the core every 5 to 10 cm, depending on its length.
  source: van_der_Merwe_et_al_2011a
  doi: https://doi.org/10.1029/2010jg001628

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: accuracy ±0.2 °C
  source: Testo_2024
  doi: https://static.testo.com/image/upload/Instruction-manual-and-Software/Instruction-manuals/testo-720-instruction-manual-7808.pdf

measurement-device-salinity_sea-ice:
  type: string
  value: TPS AQUA-C
  comment: Bulk salinity was measured for melted ice sections and for brines using a TPS AQUA-C conductivity meter.
  source: van_der_Merwe_et_al_2009
  doi: https://doi.org/10.1016/j.marchem.2009.08.001

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.4
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: 0.5% of full scale of selected range and assuming the k = 10 sensor is 0.005*80 = 0.4. 0.5% of full scale of selected range at 25 deg C. k=10 Sensor 0 PSU to 80.0 PSU.
  source: TPS_2024b
  doi: https://cdn.shopify.com/s/files/1/0552/9924/4191/files/Aqua_C_Manual.pdf
