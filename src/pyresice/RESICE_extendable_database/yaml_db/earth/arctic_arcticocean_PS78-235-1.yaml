# Anna Simson @ RWTH, December 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Arctic - Arctic Ocean - PS78/235-1

id:
  type: string
  value: PS78/235-1

location:
  type: coordinate
  value: {'N': 83.0220, 'E': -129.9037}
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276

date:
  type: string
  value: 2011-09-03
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276

type_ice:
  type: string
  value: sea ice
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276

form_sea-ice:
  type: string
  value: pack ice
  comment: transition of the ice-free ocean through first-year ice to the multi-year pack ice and back again
  source: Schauer_2012
  doi: https://doi.org/10.2312/BzPM_0649_2012

polar-region:
  type: string
  value: Arctic
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276

water-body:
  type: string
  value: Arctic Ocean
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276

campaign:
  type: string
  value: PS78
  comment: ARK-XXVI/3 (PS78 TransArc)
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.00: 272.8,
      0.10: 272.9,
      0.20: 273.2,
      0.30: 273.2,
      0.40: 273.1,
      0.50: 273.0,
      0.60: 272.9,
      0.70: 272.7,
      0.80: 272.6,
      0.90: 272.5,
      1.00: 272.5,
      1.10: 272.5,
      1.20: 272.2,
      1.30: 272.4,
      1.40: 272.2,
      1.50: 272.1,
      1.60: 272.1,
      1.70: 272.1,
      1.80: 272.1,
      1.90: 272.0,
      2.00: 272.2,
      2.10: 272.2,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Nicolaus_et_al_2012b
  doi: https://doi.org/10.1594/PANGAEA.773277
  comment: from C to K
  adjusted: 1

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.33: 0.0,
      0.43: 0.0,
      0.53: 0.1,
      0.63: 0.2,
      0.73: 0.6,
      0.83: 1.2,
      0.93: 2.6,
      1.03: 3.2,
      1.13: 3.1,
      1.23: 2.9,
      1.33: 2.9,
      1.43: 2.8,
      1.53: 2.7,
      1.63: 2.7,
      1.73: 2.7,
      1.83: 2.3,
      1.93: 2.4,
      2.03: 2.0,
      2.13: 1.9,
      2.23: 2.5,
      2.33: 3.7,
      2.43: 3.2,
      2.53: 3.1,
      2.63: 3.0,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: WTW Cond 315i
  comment: WTW Cond 3151 does not exist. The salinity core was sawed in slices of 10 cm and packed into boxes. After melting of the samples, the salinity was measured onboard with a salinometer of type WTW Cond 3151.
  source: Schauer_2012
  doi: https://doi.org/10.2312/BzPM_0649_2012

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: accuracy for ppt 0 - 42 is 0.1 ppt for measurements at 5 - 25 deg C and 0.2 ppt for measurements at 25 - 30 deg C
  source: WTW_2004
  doi: https://www.labworld.at/wp-content/uploads/2014/10/Cond_315i.pdf

thickness_sea-ice:
  type: scalar
  value: 2.68
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from lowest salinity measurement with column name depth bot
  source: Nicolaus_et_al_2012a
  doi: https://doi.org/10.1594/PANGAEA.773276
