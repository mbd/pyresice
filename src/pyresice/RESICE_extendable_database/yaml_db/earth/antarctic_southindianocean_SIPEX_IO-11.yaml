# Anna Simson @ RWTH, January 2022
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - South Indian Ocean - SIPEX_IO-11

id:
  type: string
  value: SIPEX_IO-11

location:
  type: coordinate
  value: {'N': -65.010000, 'E': 117.560000}
  source: Kramer_et_al_2010c_SIPEX_IO-11_S
  doi: https://doi.org/10.1594/PANGAEA.734463

campaign:
  type: string
  value: SIPEX
  comment: Sea Ice Physics and Ecosystem Experiment
  source: Kramer_et_al_2010c_SIPEX_IO-11_S
  doi: https://doi.org/10.1594/PANGAEA.734463

date:
  type: string
  value: 2007-10-02
  source: Kramer_et_al_2010c_SIPEX_IO-11_S
  doi: https://doi.org/10.1594/PANGAEA.734463

type_ice:
  type: string
  value: sea ice
  source: Kramer_et_al_2010c_SIPEX_IO-11_S
  doi: https://doi.org/10.1594/PANGAEA.734463

form_sea-ice:
  type: string
  value: pack ice
  comment: Sampled sea ice (Fig. 1B, Suppl. 1) was drifting pack ice except for station IO-5, which was offshore fast ice hemmed in by large icebergs.
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: All sampled ice was first-year ice, which was often rafted (Meiners et al., 2011; Worby et al., 2011).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

polar-region:
  type: string
  value: Antarctic
  source: Kramer_et_al_2010c_SIPEX_IO-11_S
  doi: https://doi.org/10.1594/PANGAEA.734463

water-body:
  type: string
  value: South Indian Ocean
  source: Kramer_et_al_2010c_SIPEX_IO-11_S
  doi: https://doi.org/10.1594/PANGAEA.734463

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.00: 266.4,
      0.06: 266.4,
      0.16: 266.7,
      0.26: 267.2,
      0.36: 267.9,
      0.46: 268.4,
      0.56: 269.0,
      0.66: 269.6,
      0.76: 270.1,
      0.86: 270.8,
      0.96: 271.2,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010d_SIPEX_IO-11_T
  doi: https://doi.org/10.1594/PANGAEA.734419
  comment: from C to K
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: Air temperature close to the snow surface, snow temperature above the snow-ice interface and ice in situ temperatures were measured using a handhold thermometer (Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Kramer_et_al_2011
  comment: Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.05: 8.1,
      0.14: 6.3,
      0.24: 4.4,
      0.34: 3.3,
      0.44: 3.1,
      0.54: 2.1,
      0.64: 3.9,
      0.74: 5.0,
      0.84: 5.5,
      0.94: 5.4,
      1.04: 7.8,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010c_SIPEX_IO-11_S
  doi: https://doi.org/10.1594/PANGAEA.734463
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: WTW LF 196
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: The sections were melted in the dark at +4 deg C, and bulk salinity was measured with a conductivity meter (WTW microprocessor conductivity meter LF 196, accuracy 0.2).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.2
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: The sections were melted in the dark at +4 deg C, and bulk salinity was measured with a conductivity meter (WTW microprocessor conductivity meter LF 196, accuracy 0.2).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

temperature_air:
  type: scalar
  value: 265.9
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526
  comment: from C to K
  adjusted: 1

measurement-device-temperature_air:
  type: string
  value: Testo 720
  comment: Air temperature close to the snow surface, snow temperature above the snow-ice interface and ice in situ temperatures were measured using a handhold thermometer (Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-temperature_air:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

thickness_sea-ice:
  type: scalar
  value: 1.01
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526

thickness_snow:
  type: scalar
  value: 0.09
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526

freeboard_sea-ice:
  type: scalar
  value: 0.02
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526
