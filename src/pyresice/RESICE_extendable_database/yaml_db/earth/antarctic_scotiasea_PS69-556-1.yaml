# Anna Simson @ RWTH, January 2022
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - Scotia Sea - PS69-556-1_WS-7

id:
  type: string
  value: PS69-556-1_WS-7

location:
  type: coordinate
  value: {'N': -59.8154000, 'E': -48.061300}
  source: Kramer_et_al_2010a_PS69/556-1_S
  doi: https://doi.org/10.1594/PANGAEA.734441

date:
  type: string
  value: 2006-09-22
  source: Kramer_et_al_2010a_PS69/556-1_S
  doi: https://doi.org/10.1594/PANGAEA.734441

campaign:
  type: string
  value: PS69
  comment: ANT-XXIII/7 (PS69 WWOS)
  source: Kramer_et_al_2010a_PS69/556-1_S
  doi: https://doi.org/10.1594/PANGAEA.734441

type_ice:
  type: string
  value: sea ice
  source: Kramer_et_al_2010a_PS69/556-1_S
  doi: https://doi.org/10.1594/PANGAEA.734441

development-stage_sea-ice_option:
  type: string
  value: second-year ice
  comment: from Table 4.1
  source: Lemke_2009
  doi: https://doi.org/10.2312/BzPM_0586_2009

form_sea-ice:
  type: string
  value: pack ice
  comment: The sampling stations in the western Weddell Sea (Fig. 1A, Suppl. 1) were pack ice, most of which probably originated from the Larsen and Ronne polynyas (Haas et al., 2009).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

development-stage_sea-ice:
  type: string
  value: multi-year ice
  comment: The samples from stations WS-4, WS-7, WS-11 and WS-21 were multi-year ice
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

polar-region:
  type: string
  value: Antarctic
  source: Kramer_et_al_2010a_PS69/556-1_S
  doi: https://doi.org/10.1594/PANGAEA.734441

water-body_option:
  type: string
  value: Weddell Sea
  comment: In the western Weddell Sea, sea ice was sampled near the South Orkney Islands and east of the tip of the Antarctic Peninsula.
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

water-body:
  type: string
  value: Scotia Sea
  comment: Scotia Sea, southwest Atlantic
  source: Kramer_et_al_2010a_PS69/556-1_S
  doi: https://doi.org/10.1594/PANGAEA.734441

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.000: 265.5,
      0.050: 266.6,
      0.150: 266.5,
      0.250: 266.8,
      0.350: 266.5,
      0.450: 267.0,
      0.550: 267.2,
      0.650: 267.2,
      0.750: 267.5,
      0.850: 267.7,
      0.950: 267.8,
      1.020: 267.7,
      1.050: 267.1,
      1.150: 267.7,
      1.250: 267.4,
      1.350: 268.0,
      1.450: 268.6,
      1.550: 268.8,
      1.600: 270.4,
      1.700: 270.4,
      1.800: 270.6,
      1.900: 270.4,
      2.000: 270.8,
      2.100: 270.8,
      2.200: 270.9,
      2.300: 271.1,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010b_PS69/556-1_T
  doi: https://doi.org/10.1594/PANGAEA.734397
  comment: from C to K
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: Air temperature close to the snow surface, snow temperature above the snow-ice interface and ice in situ temperatures were measured using a handhold thermometer (Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Kramer_et_al_2011
  comment: Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.045: 0.0,
      0.135: 0.0,
      0.225: 0.0,
      0.315: 0.0,
      0.380: 0.0,
      0.450: 0.0,
      0.550: 0.0,
      0.650: 0.0,
      0.750: 0.2,
      0.835: 0.4,
      0.910: 0.3,
      0.990: 0.2,
      1.080: 0.3,
      1.180: 0.3,
      1.280: 0.6,
      1.380: 0.7,
      1.480: 1.0,
      1.560: 1.5,
      1.630: 2.9,
      1.720: 1.7,
      1.820: 1.7,
      1.895: 3.7,
      1.970: 4.5,
      2.070: 2.6,
      2.170: 3.4,
      2.270: 2.1,
      2.345: 4.0,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010a_PS69/556-1_S
  doi: https://doi.org/10.1594/PANGAEA.734441
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: WTW LF 196
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: The sections were melted in the dark at +4 deg C, and bulk salinity was measured with a conductivity meter (WTW microprocessor conductivity meter LF 196, accuracy 0.2).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.2
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: The sections were melted in the dark at +4 deg C, and bulk salinity was measured with a conductivity meter (WTW microprocessor conductivity meter LF 196, accuracy 0.2).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

temperature_air:
  type: scalar
  value: 261.1
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Kramer_et_al_2010b_PS69/556-1_T
  doi: https://doi.org/10.1594/PANGAEA.734397
  comment: from C to K
  adjusted: 1

measurement-device-temperature_air:
  type: string
  value: Testo 720
  comment: Air temperature close to the snow surface, snow temperature above the snow-ice interface and ice in situ temperatures were measured using a handhold thermometer (Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-temperature_air:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

thickness_sea-ice:
  type: scalar
  value: 2.37
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Fig. 4.22
  source: Lemke_2009
  doi: https://doi.org/10.2312/BzPM_0586_2009

thickness_snow:
  type: scalar
  value: 1.014
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Fig. 4.22
  source: Lemke_2009
  doi: https://doi.org/10.2312/BzPM_0586_2009

freeboard_sea-ice:
  type: scalar
  value: 0.06
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Table 4.1
  source: Lemke_2009
  doi: https://doi.org/10.2312/BzPM_0586_2009
