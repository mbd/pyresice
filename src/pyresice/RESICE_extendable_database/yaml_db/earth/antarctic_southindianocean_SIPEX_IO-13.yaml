# Anna Simson @ RWTH, January 2022
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - South Indian Ocean - SIPEX_IO-13

id:
  type: string
  value: SIPEX_IO-13

location:
  type: coordinate
  value: {'N': -64.730000, 'E': 116.800000}
  source: Kramer_et_al_2010c_SIPEX_IO-13_S
  doi: https://doi.org/10.1594/PANGAEA.734465

campaign:
  type: string
  value: SIPEX
  comment: Sea Ice Physics and Ecosystem Experiment
  source: Kramer_et_al_2010c_SIPEX_IO-13_S
  doi: https://doi.org/10.1594/PANGAEA.734465

date:
  type: string
  value: 2007-10-06
  source: Kramer_et_al_2010c_SIPEX_IO-13_S
  doi: https://doi.org/10.1594/PANGAEA.734465

type_ice:
  type: string
  value: sea ice
  source: Kramer_et_al_2010c_SIPEX_IO-13_S
  doi: https://doi.org/10.1594/PANGAEA.734465

form_sea-ice:
  type: string
  value: pack ice
  comment: Sampled sea ice (Fig. 1B, Suppl. 1) was drifting pack ice except for station IO-5, which was offshore fast ice hemmed in by large icebergs.
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: All sampled ice was first-year ice, which was often rafted (Meiners et al., 2011; Worby et al., 2011).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

polar-region:
  type: string
  value: Antarctic
  source: Kramer_et_al_2010c_SIPEX_IO-13_S
  doi: https://doi.org/10.1594/PANGAEA.734465

water-body:
  type: string
  value: South Indian Ocean
  source: Kramer_et_al_2010c_SIPEX_IO-13_S
  doi: https://doi.org/10.1594/PANGAEA.734465

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.00: 267.2,
      0.03: 267.9,
      0.13: 268.4,
      0.23: 269.0,
      0.33: 269.3,
      0.43: 269.8,
      0.53: 270.3,
      0.63: 270.8,
      0.73: 271.2,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010d_SIPEX_IO-13_T
  doi: https://doi.org/10.1594/PANGAEA.734420
  comment: from C to K
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: Air temperature close to the snow surface, snow temperature above the snow-ice interface and ice in situ temperatures were measured using a handhold thermometer (Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Kramer_et_al_2011
  comment: Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.07: 8.1,
      0.18: 4.7,
      0.28: 5.6,
      0.38: 4.9,
      0.48: 4.5,
      0.58: 5.0,
      0.68: 7.3,
      0.78: 12.3,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010c_SIPEX_IO-13_S
  doi: https://doi.org/10.1594/PANGAEA.734465
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: WTW LF 196
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: The sections were melted in the dark at +4 deg C, and bulk salinity was measured with a conductivity meter (WTW microprocessor conductivity meter LF 196, accuracy 0.2).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.2
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: The sections were melted in the dark at +4 deg C, and bulk salinity was measured with a conductivity meter (WTW microprocessor conductivity meter LF 196, accuracy 0.2).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

temperature_air:
  type: scalar
  value: 265.5
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526
  comment: from C to K
  adjusted: 1

measurement-device-temperature_air:
  type: string
  value: Testo 720
  comment: Air temperature close to the snow surface, snow temperature above the snow-ice interface and ice in situ temperatures were measured using a handhold thermometer (Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C).
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

measurement-device-accuracy-temperature_air:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: Testotherm 720, Pt 100 sensor, accuracy 0.2 deg C
  source: Kramer_et_al_2011
  doi: https://doi.org/10.1016/j.dsr2.2010.10.029

thickness_sea-ice:
  type: scalar
  value: 0.805
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526

thickness_snow:
  type: scalar
  value: 0.05
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526

freeboard_sea-ice:
  type: scalar
  value: 0.07
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Kramer_et_al_2010e
  doi: https://doi.org/10.1594/PANGAEA.734526
