# Anna Simson @ RWTH, May 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - Weddell Sea - PS118_20190312_9_1

id:
  type: string
  value: PS118_20190312_9_1

location:
  type: coordinate
  value: {'N': -63.995, 'E': -55.606}
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

type_ice:
  type: string
  value: sea ice
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

form_sea-ice:
  type: string
  value: pack ice
  comment: derived from SIN_WMO_2014 using sea ice concentration from Fig. 1
  source: Arndt_et_al_2021b
  doi: https://doi.org/10.5194/tc-15-4165-2021

development-stage_sea-ice_option:
  type: string
  value: second-year ice
  comment: Based on these results, ice cores with a length of 1.53±0.07 m were classified as first-year ice, thicker ice cores as second-year ice, and thinner ice cores as young first-year ice (Fig. 1, lower right).
  source: Arndt_et_al_2021b
  doi: https://doi.org/10.5194/tc-15-4165-2021

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: from Fig. 1
  source: Arndt_et_al_2021b
  doi: https://doi.org/10.5194/tc-15-4165-2021

polar-region:
  type: string
  value: Antarctic
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

water-body_option:
  type: string
  value: northwestern Weddell Sea
  comment: Recent observations of superimposed ice and snow ice on sea ice in the northwestern Weddell Sea
  source: Arndt_et_al_2021b
  doi: https://doi.org/10.5194/tc-15-4165-2021

water-body:
  type: string
  value: Weddell Sea
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

date:
  type: string
  value: 2019-03-12
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

campaign_option:
  type: string
  value: WedIce
  comment: The data and samples of this study were collected during the interdisciplinary Weddell Sea Ice (WedIce) project on board the German icebreaker R/V Polarstern cruise PS118
  source: Arndt_et_al_2021b
  doi: https://doi.org/10.5194/tc-15-4165-2021

campaign:
  type: string
  value: PS118
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

salinity_sea-ice:
  type: tabulated
  value:
    {
      -0.135: 0.0,
      -0.045: 0.0,
      0.020: 0.0,
      0.060: 0.5,
      0.105: 1.2,
      0.155: 1.6,
      0.210: 2.3,
      0.275: 4.2,
      0.380: 2.9,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: WTW Cond 3110
  comment: Salinities were determined with a conductivity meter (pocket conductivity meter WTW 3110) with a stated accuracy of 0.5 % for each measurement.
  source: Arndt_et_al_2021b
  doi: https://doi.org/10.5194/tc-15-4165-2021

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: ± 0.1 between 5 °C and 25 °C and ± 0.2 between 25 °C and 30 °C
  source: WTW_2008a
  doi: https://www.labworld.at/wp-content/uploads/2014/10/Cond_3110.pdf

thickness_sea-ice:
  type: string
  value: 2.18
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

thickness_snow:
  type: scalar
  value: 0.18
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948

freeboard_sea-ice:
  type: scalar
  value: 0.12
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Arndt_et_al_2021a
  doi: https://doi.org/10.1594/PANGAEA.928948
