# Anna Simson @ RWTH, January 2022
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Arctic - Central Arctic Ocean - CHINARE_2018_S4

id:
  type: string
  value: CHINARE_2018_S4

location:
  type: coordinate
  value: {'N': 83.1, 'E': -167.066667}
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

date:
  type: string
  value: 2018-08-14
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

type_ice:
  type: string
  value: sea ice
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

form_sea-ice:
  type: string
  value: pack ice
  comment: derived from SIN_WMO_2014 expecting high sea ice concentration. Vast ice floes with diameters of several kilometers.
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

development-stage_sea-ice:
  type: string
  value: multi-year ice
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

campaign:
  type: string
  value: CHINARE 2018
  comment: Chinese National Arctic Research Expedition (CHINARE) programs in 2008/10/12/14/16/18
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

polar-region:
  type: string
  value: Arctic
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

water-body_option:
  type: string
  value: Pacific
  comment: Pacific sector of the Arctic
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

water-body:
  type: string
  value: Central Arctic Ocean
  comment: from Fig. 1
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

temperature_air:
  type: tabulated
  value: 274.15
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867
  comment: from C to K
  adjusted: 1

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.10: 0.1,
      0.20: 0.1,
      0.30: 0.2,
      0.40: 0.3,
      0.50: 0.8,
      0.60: 1.0,
      0.70: 1.2,
      0.80: 1.3,
      0.90: 1.2,
      1.00: 2.9,
      1.10: 4.3,
      1.20: 1.2,
      1.28: 1.3,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867
  comment: from psu without changed
  adjusted: 0

measurement-device-salinity_sea-ice:
  type: string
  value: salinometer
  comment: The salinity samples were melted aboard the ship, and a salinometer with an accuracy of 0.1 practical salinity unit (psu) was used to measure the salinity at a room temperature.
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  source: Wang_et_al_2020b
  doi: https://doi.org/10.1029/2020JC016371

thickness_snow:
  type: scalar
  value: 0.06
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

freeboard_sea-ice:
  type: scalar
  value: 0.034
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867

thickness_sea-ice:
  type: scalar
  value: 1.280
  dev_pdf: Gauss
  dev_value: 0
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  source: Wang_et_al_2020a
  doi: https://doi.org/10.5281/zenodo.3779867
