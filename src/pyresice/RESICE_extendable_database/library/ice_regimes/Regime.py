###############################################################
# 
# Generic Regime class 
# Julia @ RWTH, March 2020
# Qian @ RWTH, July 2020
#
################################################################

# imports from system libraries if necessary
import os

# Python imports
import numpy as np
import pandas as pd
import yaml as yaml


# import parent class if needed
# from . import ????
# PrettySafeLoader for loading tuple type from .yaml files
class PrettySafeLoader(yaml.CSafeLoader):
    def construct_python_tuple(self, node):
        return tuple(self.construct_sequence(node))


PrettySafeLoader.add_constructor(
    u'tag:yaml.org,2002:python/tuple',
    PrettySafeLoader.construct_python_tuple)


class Regime:
    NAME_DEFAULT = "Default"

    def __init__(self, name=None, file_name=None):
        # defaulted instance arguments
        self.separator = ": "
        self.populated = False
        self.ice_props = pd.DataFrame()
        self.ice_propsfile = None
        self.site = pd.DataFrame()
        self.site_file = None
        # instance arguments that get optionally passed over by __init__
        self.name = str(name) if name else self.NAME_DEFAULT
        self.figures = {}
        self.interpl_dict = {}  # interploation dictionary for storing interpolation functions
        self.profile_data = None  # storing 2D profile data
        if file_name:
            self.load_ice_props(file_name=file_name)

    def __str__(self):
        out = 'REGIME name' + self.separator + self.name + '\n'
        if not self.populated:
            out += 'Ice regime incomplete!'
        else:
            out += 'Ice regime' + self.separator + self.name + '\n'
            out += self.ice_props.__repr__()
        out += '\n'
        return out


    def load_ice_props(self, file_name=None):
        # read *.yaml file and save as pandas dataframe
        if file_name:
            with open(file_name, encoding='utf-8') as file:
                yaml_data = yaml.load(file, Loader=PrettySafeLoader)
                if 'name' in yaml_data.keys():
                    self.name = yaml_data.pop('name')
                if 'figures' in yaml_data.keys():
                    self.figures = yaml_data.pop('figures')
                self.ice_props = pd.DataFrame.from_dict(yaml_data, orient='index').T
                self.ice_propsfile = file_name
                self.populated = True
                # print('Ice properties loaded from YAML file:', file_name, sep = self.separator)
                # print('\n')
        else:
            print('Please specify YAML file to load ice properties')
            print('\n')

