import numpy as np
import os
import sys
import pickle
# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))
# Change the working directory to the script's directory
os.chdir(script_dir)
parent_dir = os.path.dirname(os.getcwd())
sys.path.append(parent_dir)
mapping_path = os.path.join(parent_dir, 'automatic_enrichment', 'seaVoXmapping') #Add the icedatahub path to the sys.path
sys.path.append(mapping_path)

def enrich_mean_distance(df):
    '''
    This function takes the list of objects and enriches the mean distance of the measurements of the sea ice properties temperature and salinity
    '''
    for id in df['id'].unique():
        indices = df[df['id'] == id].index
        depth_salinity_sea_ice = df.loc[indices, 'depth_salinity_sea_ice [m]'].tolist()
        depth_temperature_sea_ice = df.loc[indices, 'depth_temperature_sea_ice [m]'].tolist()
        df.loc[indices, 'mean_distance_measurements_salinity_sea_ice [m]'] = compute_mean_distance(depth_salinity_sea_ice)
        df.loc[indices, 'mean_distance_measurements_temperature_sea_ice [m]'] = compute_mean_distance(depth_temperature_sea_ice)
    return df

def compute_mean_distance(depth_list):
    '''
    Function to compute the mean distance between items of a list
    '''

    # Compute the mean distance of the measurements
    lowest_key = min(depth_list)
    highest_key = max(depth_list)
    total_distance = highest_key - lowest_key
    mean_distance = total_distance / len(depth_list)
    
    return np.round(mean_distance,3)

def enrich_mean(df):
    '''
    This function takes the list of objects and enriches the spatial resolution of the sea ice properties
    '''
    for id in df['id'].unique():
        indices = df[df['id'] == id].index
        salinity_sea_ice = df.loc[indices, 'salinity_sea_ice [ppt]'].tolist()
        temperature_sea_ice = df.loc[indices, 'temperature_sea_ice [K]'].tolist()
        df.loc[indices, 'mean_salinity_sea_ice [ppt]'] = round(np.mean(np.array((salinity_sea_ice))),2)
        df.loc[indices, 'mean_temperature_sea_ice [K]'] = round(np.mean(np.array((temperature_sea_ice))),2)
    return df

def enrich_development_stage_SIN(df):
    '''
    '''
    df['development_stage_SIN_level_1_sea_ice'] =  df.apply(lambda row: development_stage_sea_ice_SIN(row['thickness_sea_ice [m]'], row['development_stage_sea_ice'])[0], axis=1)
    df['development_stage_SIN_level_2_sea_ice'] =  df.apply(lambda row: development_stage_sea_ice_SIN(row['thickness_sea_ice [m]'], row['development_stage_sea_ice'])[1], axis=1)
    df['INFO_residual_ice'] =  df.apply(lambda row: development_stage_sea_ice_SIN(row['thickness_sea_ice [m]'], row['development_stage_sea_ice'])[2], axis=1)
    df['INFO_SIN'] = df.apply(lambda row: development_stage_sea_ice_SIN(row['thickness_sea_ice [m]'], row['development_stage_sea_ice'])[3], axis=1)
    return df
    
def development_stage_sea_ice_SIN(thickness_sea_ice, age_sea_ice=None):
    """
    Determine the development stage (Level 1 and Level 2) of sea ice based on thickness 
    and optionally the provided development stage (age_sea_ice). Uses the Sea Ice Nomenclature (SIN).
    """
    WMO_cats_depth_ranges = {
    "nilas": {"dark nilas": [0.0, 0.05], "light nilas": [0.05, 0.1]},
    "young ice": {"grey ice": [0.1, 0.15], "grey-white ice": [0.15, 0.3]},
    "first-year ice": {
        "thin first-year ice": [0.3, 0.7],
        "medium first-year ice": [0.7, 1.2],
        "thick first-year ice": [1.2, 2.0],
        },
    "old ice": {
        "residual ice": [0.3, 1.8],
        "second-year ice": [2.0, 2.5],
        "multi-year ice": [2.5, 4.0],
        }
    }

    INFO_SIN = "None"
    INFO_residual_ice = "None"

    # Helper function to derive levels based on thickness
    def development_stage_sea_ice_thickness(WMO_cats_depth_ranges, thickness_sea_ice):
        INFO_residual_ice = (
            "CAUTION! The thickness intersects with the development stage of Residual Ice. "
            "Please verify the development stage."
            if 0.3 <= thickness_sea_ice <= 1.8
            else "None"
        )
        
        for level_1, level_2_dict in WMO_cats_depth_ranges.items():
            for level_2, range_vals in level_2_dict.items():
                if level_2 == "residual ice":
                    continue  # Skip "Residual ice"
                if range_vals[0] <= thickness_sea_ice <= range_vals[1]:
                    return level_1, level_2, INFO_residual_ice 

        # If no valid match is found
        return None, None, "CAUTION! Thickness does not match any defined category (excluding Residual Ice). Please verify.", INFO_SIN
    if age_sea_ice == "pancake ice" and thickness_sea_ice < 0.1:
        return "pancake ice", "None", INFO_residual_ice, INFO_SIN
    # Main logic
    if age_sea_ice:
        # If age_sea_ice matches Level 1
        if age_sea_ice in WMO_cats_depth_ranges:
            level_1 = age_sea_ice
            for level_2, range_vals in WMO_cats_depth_ranges[level_1].items():
                if range_vals[0] <= thickness_sea_ice <= range_vals[1]:
                    return level_1, level_2, INFO_residual_ice, INFO_SIN, 
            # If thickness doesn't match the specified Level 1
            INFO_SIN = (
                "CAUTION! SIN thickness does not match Level 2 categories for the given Level 1. "
                "Levels derived based on thickness only."
            )
            level_1, level_2, INFO_residual_ice = development_stage_sea_ice_thickness(WMO_cats_depth_ranges, thickness_sea_ice)
            return level_1, level_2, INFO_residual_ice , INFO_SIN

        # If age_sea_ice matches Level 2
        for level_1, level_2_dict in WMO_cats_depth_ranges.items():
            if age_sea_ice in level_2_dict:
                level_2 = age_sea_ice
                if level_2_dict[level_2][0] <= thickness_sea_ice <= level_2_dict[level_2][1]:
                    return level_1, level_2, INFO_residual_ice, INFO_SIN
                INFO_SIN = (
                    "CAUTION! SIN thickness does not match the provided Level 2 category. "
                    "Levels derived based on thickness only."
                )
                level_1, level_2, INFO_residual_ice = development_stage_sea_ice_thickness(WMO_cats_depth_ranges, thickness_sea_ice)
                return level_1, level_2, INFO_residual_ice , INFO_SIN

    # If no valid age_sea_ice is provided
    INFO_SIN = "CAUTION! No valid development stage provided in sources. Levels derived based on thickness only. Please verify the development stage"
    level_1, level_2, INFO_residual_ice = development_stage_sea_ice_thickness(WMO_cats_depth_ranges, thickness_sea_ice)
    return level_1, level_2, INFO_residual_ice , INFO_SIN

def enrich_seaVoX(df):
    '''
    This function automatically generates the sea and ocean of SeaVoX controlled vocabulary from the latitude and longitude
    '''
    # load Sea_SeaVox_dict from file
    with open(os.path.join(mapping_path, 'Sea_SeaVoX_dict.pickle'), 'rb') as handle:
        Sea_SeaVoX_dict = pickle.load(handle)

    # Apply Sea_SeaVox_dict to get Ocean from Latitudes and Longitudes and add to sea_ice_per_core
    df['sea_SeaVoX'] = df.apply(lambda row: Sea_SeaVoX_dict[(row['latitude'], row['longitude'])], axis=1)

    # load Ocean_SeaVox_dict from file
    with open(os.path.join(mapping_path, 'Ocean_SeaVoX_dict.pickle'), 'rb') as handle:
        Ocean_SeaVoX_dict = pickle.load(handle)

    # Apply Ocean_SeaVox_dict to get Ocean from Latitudes and Longitudes and add to sea_ice_per_core
    df['ocean_SeaVoX'] = df.apply(lambda row: Ocean_SeaVoX_dict[(row['latitude'], row['longitude'])], axis=1)
    return df
