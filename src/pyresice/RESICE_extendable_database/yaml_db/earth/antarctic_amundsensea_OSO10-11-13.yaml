# Anna Simson @ RWTH, December 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - Amundsen Sea - OSO10-11-13

id:
  type: string
  value: OSO10-11-13

location:
  type: coordinate
  value: {'N': -73.26, 'E': -139.2}
  source: Torstensson_et_al_2018a
  doi: https://doi.org/10.1594/PANGAEA.924295

date:
  type: string
  value: 2011-01-04
  comment: from Table 1
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

campaign:
  type: string
  value: OSO 10/11
  comment: during the Oden Southern Ocean cruise from December 2010 to January 2011
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

polar-region:
  type: string
  value: Antarctic
  source: Torstensson_et_al_2018a
  doi: https://doi.org/10.1594/PANGAEA.924295

water-body:
  type: string
  value: Amundsen Sea
  comment: from Fig. 1
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

type_ice:
  type: string
  value: sea ice
  source: Torstensson_et_al_2018a
  doi: https://doi.org/10.1594/PANGAEA.924295

form_sea-ice:
  type: string
  value: pack ice
  comment: All sea-ice samples in this study came from first-year pack ice.
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: All sea-ice samples in this study came from first-year pack ice.
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.05: 272.7,
      0.15: 272.1,
      0.25: 272.3,
      0.35: 272.2,
      0.45: 272.2,
      0.55: 272.1,
      0.65: 272.2,
      0.75: 272.4,
      0.95: 272.7,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Torstensson_et_al_2018a
  doi: https://doi.org/10.1594/PANGAEA.924295
  comment: from C to K and accuracy correction
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Ama-digit ad 15 th
  comment: Sea-ice temperature was recorded immediately after recovery of physicochemical cores using a digital thermistor (Ama-digit ad 15 th, Amarell GmbH & Co, Kreuzwertheim, Germany) with an accuracy of ±0.1°C.
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.1
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.05: 1.3,
      0.15: 2.3,
      0.25: 3.0,
      0.35: 3.4,
      0.45: 3.6,
      0.55: 2.9,
      0.65: 2.3,
      0.75: 2.7,
      0.95: 4.3,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Torstensson_et_al_2018a
  doi: https://doi.org/10.1594/PANGAEA.924295
  adjusted: 0

measurement-device-salinity_sea-ice_option:
  type: string
  value: Cond 310i
  comment: Measurement device Cond 310i is not available from WTW (Xylem) website, also not after calling so it maybe WTW cond 315i or WTW cond3110. Salinity in the melted sea-ice sections (bulk salinity) was measured using a conductivity meter (Cond 310i, WTW GmbH, Weilheim, Germany) with a resolution and accuracy of ±0.05 units.
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.05
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: resolution and accuracy of ±0.05 units
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

thickness_sea-ice:
  type: scalar
  value: 0.85
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Table 1
  source: Torstensson_et_al_2018b
  doi: https://doi.org/10.1371/journal.pone.0195587

thickness_snow:
  type: scalar
  value: 0.07
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: Depth ice/snow column
  source: Torstensson_et_al_2018a
  doi: https://doi.org/10.1594/PANGAEA.924295
