# Anna Simson @ RWTH, September 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Antarctica - Weddell Sea - ISPOL_03

id:
  type: string
  value: ISPOL_03

location:
  type: coordinate
  value: {'N': -68.0433, 'E': -55.2050}
  source: Lannuzel_2016c
  doi: https://doi.org/10.1594/PANGAEA.865027

type_ice:
  type: string
  value: sea ice
  source: Lannuzel_2016c
  doi: https://doi.org/10.1594/PANGAEA.865027

form_sea-ice:
  type: string
  value: pack ice
  comment: From 29 November until the 30 December, one first-year pack ice station was sampled every 5 days in order to follow the temporal evolution of Fe concentrations.
  source: Lannuzel_et_al_2008
  doi: https://doi.org/10.1016/j.marchem.2007.10.006

development-stage_sea-ice:
  type: string
  value: first-year ice
  comment: From 29 November until the 30 December, one first-year pack ice station was sampled every 5 days in order to follow the temporal evolution of Fe concentrations.
  source: Lannuzel_et_al_2008
  doi: https://doi.org/10.1016/j.marchem.2007.10.006

polar-region:
  type: string
  value: Antarctic
  source: Lannuzel_2016c
  doi: https://doi.org/10.1594/PANGAEA.865027

water-body_option:
  type: string
  value: Western Weddell Sea
  comment: Samples of sea ice, snow, brine and underlying seawater were collected in the western Weddell pack ice
  source: Lannuzel_et_al_2008
  doi: https://doi.org/10.1016/j.marchem.2007.10.006

water-body:
  type: string
  value: Weddell Sea
  source: Lannuzel_2016c
  doi: https://doi.org/10.1594/PANGAEA.865027

campaign:
  type: string
  value: ISPOL
  source: Lannuzel_2016c
  doi: https://doi.org/10.1594/PANGAEA.865027

date:
  type: string
  value: 2004-12-09
  source: Lannuzel_2016c
  doi: https://doi.org/10.1594/PANGAEA.865027

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.05: 272.0,
      0.15: 271.8,
      0.25: 271.4,
      0.35: 271.4,
      0.45: 271.3,
      0.55: 271.1,
      0.65: 271.4,
      0.75: 271.3,
      0.85: 271.4,
      0.90: 271.4,
      0.95: 271.4,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lannuzel_2016c
  doi: https://doi.org/10.1594/PANGAEA.865027
  comment: from C to K and accuracy correction
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: In-situ ice temperatures were measured on site using a calibrated probe (TESTO 720) inserted every 5 or 10 cm along a freshly sampled core.
  source: Lannuzel_et_al_2008
  doi: https://doi.org/10.1016/j.marchem.2007.10.006

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: accuracy ±0.2 °C
  source: Testo_2024
  doi: https://static.testo.com/image/upload/Instruction-manual-and-Software/Instruction-manuals/testo-720-instruction-manual-7808.pdf

thickness_sea-ice:
  type: scalar
  value: 0.90
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Table 1
  source: Lannuzel_et_al_2008
  doi: https://doi.org/10.1016/j.marchem.2007.10.006

thickness_snow:
  type: scalar
  value: 0.25
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Table 1
  source: Lannuzel_et_al_2008
  doi: https://doi.org/10.1016/j.marchem.2007.10.006
