# Anna Simson @ RWTH, September 2021
#
# name:
#  - type                                                  # [ scalar, tabulated ]
#  - value                                                 # float
#  - dev_pdf                                             # Gauss or other parametrized or tabulated PDF
#  - dev_value                                          # hyperparameters of PDF
#  - unit_str                                              # standard string to inidate unit
#  - unit [ kg m s K A mol cd ]                # unit in systematically documented SI units
#  - variable                                             # function argument
#  - variable_unit [ kg m s K A mol cd ] # unit in systematically documented SI units
#  - variable_unit_str                               # standard string to indicate variable_unit
#  - comment                                      # specific details on how the field was filled can be sentence from source or comment from data compiler
#  - source [ string ]                                # data source
#  - doi [ string ]                                      # doi or url of the data source

name: Arctic - Lincoln Sea - CASIMBO-CORE-4_12

id:
  type: string
  value: CASIMBO-CORE-4_12

location:
  type: coordinate
  value: {'N': 86.110320, 'E': -78.051570}
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370

date:
  type: string
  value: 2012-05-04
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370

type_ice:
  type: string
  value: sea ice
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370

form_sea-ice:
  type: string
  value: pack ice
  comment: from Fig. 1 b) and c)
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

development-stage_sea-ice:
  type: string
  value: first-year ice
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370

polar-region:
  type: string
  value: Arctic
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370

water-body:
  type: string
  value: Arctic Ocean
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370

campaign:
  type: string
  value: CASIMBO
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370

temperature_sea-ice:
  type: tabulated
  value:
    {
      0.085: 264.1,
      0.220: 264.7,
      0.320: 265.1,
      0.420: 265.5,
      0.520: 265.9,
      0.620: 266.3,
      0.720: 266.7,
      0.820: 267.2,
      0.920: 267.6,
      1.020: 268.0,
      1.120: 268.4,
      1.220: 268.8,
      1.320: 269.2,
      1.420: 269.6,
      1.520: 270.1,
      1.620: 270.5,
      1.720: 270.9,
    }
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370
  comment: accuracy correction
  adjusted: 1

measurement-device-temperature_sea-ice:
  type: string
  value: Testo 720
  comment: Internal ice temperatures were measured on texture cores by drilling holes and inserting a thermometer (Testo 720) immediately after core extraction. Temperatures were measured from surface to bottom at intervals of 0.1 m (cores 1–10, 3–10 and 4–10) and 0.5 m (core 5–10).
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

calculation-method-temperature_sea-ice:
  type: string
  value: linear interpolation
  comment: In 2012, only ice surface temperature (depth 0.1 m) was measured. For these cores the internal ice temperatures were linearly interpolated between the surface and assumed (theoretical) bottom temperature of -1.78°C with typical surface water salinities in the Lincoln Sea of ~32
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

measurement-device-accuracy-temperature_sea-ice:
  type: scalar
  value: 0.2
  unit_str: K
  unit: [0 0 0 1 0 0 0]
  comment: accuracy ±0.2 °C
  source: Testo_2024
  doi: https://static.testo.com/image/upload/Instruction-manual-and-Software/Instruction-manuals/testo-720-instruction-manual-7808.pdf

salinity_sea-ice:
  type: tabulated
  value:
    {
      0.085: 0.2,
      0.220: 0.6,
      0.320: 1.0,
      0.420: 1.5,
      0.520: 2.4,
      0.620: 2.7,
      0.720: 2.7,
      0.820: 3.2,
      0.920: 3.9,
      1.020: 4.9,
      1.120: 4.7,
      1.220: 4.0,
      1.320: 4.3,
      1.420: 4.3,
      1.520: 4.2,
      1.620: 4.2,
      1.720: 4.7,
    }
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  variable: depth ice/snow
  variable_unit_str: m
  variable_unit: [0 1 0 0 0 0 0]
  source: Lange_et_al_2015_CASIMBO-CORE-4_12
  doi: https://doi.org/10.1594/PANGAEA.842370
  comment: accuracy correction
  adjusted: 1

measurement-device-salinity_sea-ice:
  type: string
  value: WTW Cond 3300i
  comment: For each section, the ice remaining after cutting was put into plastic containers, melted and analyzed for bulk salinity using a salinometer (WTW 3300i). Bulk salinities are reported in parts per thousand (ppt).
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

measurement-device-accuracy-salinity_sea-ice:
  type: scalar
  value: 0.1
  unit_str: ppt
  unit: [0 0 0 0 0 0 0]
  comment: Between 0 and 42 ppt accuracy is ± 0.1 at 5 deg C ... 25 deg C and ± 0.2 between 25 deg C and 30 deg C
  source: WTW_2008b
  doi: https://www.labworld.at/wp-content/uploads/2017/09/Bedienungsanleitung-WTW-Cond-3300i-3400i.pdf

thickness_sea-ice:
  type: scalar
  value: 1.77
  unit_str: m
  unit: [0 1 0 0 0 0 0]
  comment: from Table 3
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418

thickness_snow:
  type: scalar
  value: 0.47
  unit_str: m
  comment: from Table 3
  source: Lange_et_al_2015b
  doi: https://doi.org/10.1371/journal.pone.0122418
