import numpy as np
import pickle
import pandas as pd 
import seaborn as sns
import matplotlib.pyplot as plt
import os
# Get the directory where the script is located
script_dir = os.path.dirname(os.path.abspath(__file__))
# Change the working directory to the script's directory
os.chdir(script_dir)
parent_dir = os.path.dirname(os.path.dirname(os.path.dirname(script_dir)))
from pyresice.read.read_funcs import load_yaml_files

def get_key_from_value(my_dict, search_value):
    '''
    Returns the key of the first matching value found in the dictionary.
    '''
    return next((key for key, value in my_dict.items() if value == search_value), None)

def define_direct_source(source_prop, primary_source, secondary_source, tertiary_source):
    '''
    gives each source type a score based in doi
    '''
    if source_prop is None or source_prop is np.nan or source_prop == 'None': # if the source is not available
        source_prop_12 = 1.0 
    elif source_prop in primary_source: # if the source is primary
        source_prop_12 =0.0
    elif source_prop in secondary_source: # if the source is secondary
        source_prop_12 = 0.2
    elif source_prop in tertiary_source: # if the source is tertiary
        source_prop_12 = 0.4
    else:
        print(source_prop)
    return source_prop_12

def load_source_group_definition():
    file_path1 = os.path.join(os.path.dirname(__file__), "primary_source.pkl")
    file_path2 = os.path.join(os.path.dirname(__file__), "secondary_source.pkl")
    file_path3 = os.path.join(os.path.dirname(__file__), "tertiary_source.pkl")

    # Load the  source files and retrieve the source list
    with open(file_path1, "rb") as file:
        primary_source = pickle.load(file)

    with open(file_path2, "rb") as file:
        secondary_source = pickle.load(file)

    with open(file_path3, "rb") as file:
        tertiary_source = pickle.load(file)
    return primary_source, secondary_source, tertiary_source

def update_source_group_definition():
    '''
    Updates the source group definition
    Add new source in the corresponding list
    '''
    primary_source = ['https://doi.org/10.26179/5CAD74D6A3179', 'https://doi.org/10.26179/5D9AC6A8CECC6', 'https://doi.org/10.5281/zenodo.3779867', 'https://doi.org/10.1594/PANGAEA.818523', 'https://doi.org/10.1594/PANGAEA.811539', 'https://doi.org/10.5281/zenodo.6997448', 'https://doi.org/10.5281/zenodo.6997630', 'https://doi.org/10.1594/PANGAEA.818523', 'https://doi.org/10.1594/PANGAEA.734389', 'https://doi.org/10.1594/PANGAEA.734391', 'https://doi.org/10.1594/PANGAEA.734392', 'https://doi.org/10.1594/PANGAEA.734393', 'https://doi.org/10.1594/PANGAEA.734394', 'https://doi.org/10.1594/PANGAEA.734395', 'https://doi.org/10.1594/PANGAEA.734396', 'https://doi.org/10.1594/PANGAEA.734397', 'https://doi.org/10.1594/PANGAEA.734401', 'https://doi.org/10.1594/PANGAEA.734402', 'https://doi.org/10.1594/PANGAEA.734403', 'https://doi.org/10.1594/PANGAEA.734404', 'https://doi.org/10.1594/PANGAEA.734405', 'https://doi.org/10.1594/PANGAEA.734406', 'https://doi.org/10.1594/PANGAEA.734407', 'https://doi.org/10.1594/PANGAEA.734408', 'https://doi.org/10.1594/PANGAEA.734409', 'https://doi.org/10.1594/PANGAEA.734410', 'https://doi.org/10.1594/PANGAEA.734411', 'https://doi.org/10.1594/PANGAEA.734412', 'https://doi.org/10.1594/PANGAEA.734413', 'https://doi.org/10.1594/PANGAEA.734417', 'https://doi.org/10.1594/PANGAEA.734418', 'https://doi.org/10.1594/PANGAEA.734419', 'https://doi.org/10.1594/PANGAEA.734420', 'https://doi.org/10.1594/PANGAEA.734421', 'https://doi.org/10.1594/PANGAEA.734422', 'https://doi.org/10.1594/PANGAEA.734423', 'https://doi.org/10.1594/PANGAEA.734424', 'https://doi.org/10.1594/PANGAEA.734425', 'https://doi.org/10.1594/PANGAEA.734426', 'https://doi.org/10.1594/PANGAEA.734427', 'https://doi.org/10.1594/PANGAEA.734428', 'https://doi.org/10.1594/PANGAEA.734439', 'https://doi.org/10.1594/PANGAEA.734440', 'https://doi.org/10.1594/PANGAEA.734441', 'https://doi.org/10.1594/PANGAEA.734442', 'https://doi.org/10.1594/PANGAEA.734443', 'https://doi.org/10.1594/PANGAEA.734444', 'https://doi.org/10.1594/PANGAEA.734445', 'https://doi.org/10.1594/PANGAEA.734446', 'https://doi.org/10.1594/PANGAEA.734447', 'https://doi.org/10.1594/PANGAEA.734448', 'https://doi.org/10.1594/PANGAEA.734449', 'https://doi.org/10.1594/PANGAEA.734450', 'https://doi.org/10.1594/PANGAEA.734451', 'https://doi.org/10.1594/PANGAEA.734452', 'https://doi.org/10.1594/PANGAEA.734453', 'https://doi.org/10.1594/PANGAEA.734455', 'https://doi.org/10.1594/PANGAEA.734456', 'https://doi.org/10.1594/PANGAEA.734457', 'https://doi.org/10.1594/PANGAEA.734458', 'https://doi.org/10.1594/PANGAEA.734459', 'https://doi.org/10.1594/PANGAEA.734460', 'https://doi.org/10.1594/PANGAEA.734461', 'https://doi.org/10.1594/PANGAEA.734462', 'https://doi.org/10.1594/PANGAEA.734463', 'https://doi.org/10.1594/PANGAEA.734465', 'https://doi.org/10.1594/PANGAEA.734466', 'https://doi.org/10.1594/PANGAEA.734468', 'https://doi.org/10.1594/PANGAEA.734469', 'https://doi.org/10.1594/PANGAEA.734470', 'https://doi.org/10.1594/PANGAEA.734471', 'https://doi.org/10.1594/PANGAEA.734472', 'https://doi.org/10.1594/PANGAEA.734473', 'https://doi.org/10.1594/PANGAEA.734474', 'https://doi.org/10.1594/PANGAEA.734476', 'https://doi.org/10.1594/PANGAEA.734477', 'https://doi.org/10.1594/PANGAEA.734526', 'https://doi.org/10.1594/PANGAEA.773276', 'https://doi.org/10.1594/PANGAEA.773277', 'https://doi.org/10.1594/PANGAEA.818523', 'https://doi.org/10.1594/PANGAEA.818647', 'https://doi.org/10.1594/PANGAEA.842359', 'https://doi.org/10.1594/PANGAEA.842360', 'https://doi.org/10.1594/PANGAEA.842361', 'https://doi.org/10.1594/PANGAEA.842362', 'https://doi.org/10.1594/PANGAEA.842363', 'https://doi.org/10.1594/PANGAEA.842364', 'https://doi.org/10.1594/PANGAEA.842365', 'https://doi.org/10.1594/PANGAEA.842366', 'https://doi.org/10.1594/PANGAEA.842367', 'https://doi.org/10.1594/PANGAEA.842368', 'https://doi.org/10.1594/PANGAEA.842369', 'https://doi.org/10.1594/PANGAEA.842370', 'https://doi.org/10.1594/PANGAEA.842371', 'https://doi.org/10.1594/PANGAEA.842372', 'https://doi.org/10.1594/PANGAEA.842373', 'https://doi.org/10.1594/PANGAEA.842374', 'https://doi.org/10.1594/PANGAEA.842375', 'https://doi.org/10.1594/PANGAEA.842376', 'https://doi.org/10.1594/PANGAEA.845798', 'https://doi.org/10.1594/PANGAEA.865023', 'https://doi.org/10.1594/PANGAEA.865026', 'https://doi.org/10.1594/PANGAEA.865027', 'https://doi.org/10.1594/PANGAEA.865031', 'https://doi.org/10.1594/PANGAEA.865035', 'https://doi.org/10.1594/PANGAEA.886593', 'https://doi.org/10.1594/PANGAEA.919474', 'https://doi.org/10.1594/PANGAEA.924295', 'https://doi.org/10.1594/PANGAEA.928948']
    secondary_source = ['https://doi.org/10.1002/2014JC010620', 'https://doi.org/10.1016/j.dsr2.2010.10.029', 'https://doi.org/10.1016/j.dsr2.2010.10.036', 'https://doi.org/10.1016/j.dsr2.2014.12.003', 'https://doi.org/10.1016/j.marchem.2006.06.010', 'https://doi.org/10.1016/j.marchem.2007.10.006', 'https://doi.org/10.1016/j.marchem.2009.08.001', 'https://doi.org/10.1017/jog.2023.21', 'https://doi.org/10.1029/2010JC006614', 'https://doi.org/10.1029/2010jg001628', 'https://doi.org/10.1029/2019jc015221', 'https://doi.org/10.1029/2020GL088898', 'https://doi.org/10.1029/2020JC016371', 'https://doi.org/10.1038/s41467-018-03825-5', 'https://doi.org/10.12952/journal.elementa.000130', 'https://doi.org/10.1371/journal.pone.0122418', 'https://doi.org/10.1371/journal.pone.0195587', 'https://doi.org/10.2312/BzPM_0586_2009', 'https://doi.org/10.2312/BzPM_0649_2012', 'https://doi.org/10.2312/BzPM_0732_2019', 'https://doi.org/10.26179/5CAD74D6A3179', 'https://doi.org/10.26179/5D9AC6A8CECC6', 'https://doi.org/10.5194/tc-15-4165-2021', 'https://doi.org/10.5194/tc-16-2899-2022', 'https://doi.org/10.1109/TGRS.2010.2043954']
    tertiary_source = ['https://www.fondriest.com/pdf/hach_sension5_manual.pdf', 'https://www.labworld.at/wp-content/uploads/2014/10/Cond_3110.pdf', 'https://www.labworld.at/wp-content/uploads/2014/10/Cond_315i.pdf', 'https://www.labworld.at/wp-content/uploads/2017/09/Bedienungsanleitung-WTW-Cond-3300i-3400i.pdf', 'https://www.novatech-usa.com/pdf/Control%20Company%204000%20Instruction%20Manual.pdf', 'https://static.testo.com/image/upload/Instruction-manual-and-Software/Instruction-manuals/testo-720-instruction-manual-7808.pdf', 'https://cdn.shopify.com/s/files/1/0552/9924/4191/files/Aqua_C_Manual.pdf', 'https://cdn.shopify.com/s/files/1/0552/9924/4191/files/WP-84.pdf']
    # write new list to files
    with open('primary_source.pkl', 'wb') as file:
        pickle.dump(primary_source, file)

    with open('secondary_source.pkl', 'wb') as file:
        pickle.dump(secondary_source, file)

    with open('tertiary_source.pkl', 'wb') as file:
        pickle.dump(tertiary_source, file)
    return

def create_tabular_db_source_categories(YAML_file_list, data_list, primary_source, secondary_source, tertiary_source):
    source_core_data=pd.DataFrame()
    source_sea_ice_per_core =pd.DataFrame()
    coordinates_list = []
    acc_list = []
    key_list =[]
    source_dict = {}
    name_dict = {}
    doi_dict = {}
    campaign_dict = {}
    source_doi = []
    indexx =0
    for i in range(len(YAML_file_list)):
        key = data_list[i]
        try:
            salinity_sea_ice_source = key.ice_props['salinity_sea-ice']['doi']
            source_10_salinity_sea_ice = define_direct_source(salinity_sea_ice_source, primary_source, secondary_source, tertiary_source)
        except KeyError:
            salinity_sea_ice_source = None
            source_10_salinity_sea_ice = 1.0
            pass
        
        try:
            temperature_sea_ice_source = key.ice_props['temperature_sea-ice']['doi']
            source_10_temperature_sea_ice = define_direct_source(temperature_sea_ice_source, primary_source, secondary_source, tertiary_source)
        except KeyError:
            temperature_sea_ice_source = None
            source_10_temperature_sea_ice = 1.0
            pass

        try:
            temperature_air_source = key.ice_props['temperature_air']['doi']
            source_10_temperature_air = define_direct_source(temperature_air_source, primary_source, secondary_source, tertiary_source)
        except KeyError:
            temperature_air_source = None
            source_10_temperature_air = 1.0
            pass

        try:
            meas_dev_temperature_air_source = key.ice_props['measurement-device-temperature_air']['doi']
            source_10_measurement_device_temperature_air = define_direct_source(meas_dev_temperature_air_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            source_10_measurement_device_temperature_air = 1.0
            meas_dev_temperature_air_source = None
            pass

        try:
            acc_temperature_air_source = key.ice_props['measurement-device-accuracy-temperature_air']['doi']
            source_10_acc_temperature_air = define_direct_source(acc_temperature_air_source, primary_source, secondary_source, tertiary_source)   
        except KeyError:
            acc_temperature_air_source = None
            source_10_acc_temperature_air = 1.0
        
        try:
            meas_dev_salinity_sea_ice_source = key.ice_props['measurement-device-salinity_sea-ice']['doi']
            source_10_measurement_device_salinity_sea_ice = define_direct_source(meas_dev_salinity_sea_ice_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            meas_dev_salinity_sea_ice_source = None
            source_10_measurement_device_salinity_sea_ice = 1.0
            pass

        try:
            meas_dev_salinity_sea_ice_option_source = key.ice_props['measurement-device-salinity_sea-ice_option']['doi']
            source_10_measurement_device_salinity_sea_ice_option = define_direct_source(meas_dev_salinity_sea_ice_option_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            meas_dev_salinity_sea_ice_option_source = None
            source_10_measurement_device_salinity_sea_ice_option = 1.0
            pass

        try:
            acc_salinity_sea_ice_source = key.ice_props['measurement-device-accuracy-salinity_sea-ice']['doi']
            source_10_acc_salinity_sea_ice = define_direct_source(acc_salinity_sea_ice_source, primary_source, secondary_source, tertiary_source)   
            acc_list.append(acc_salinity_sea_ice_source)
        except KeyError:
            acc_salinity_sea_ice_source = None
            source_10_acc_salinity_sea_ice = 1.0
            pass

        try:
            acc_salinity_sea_ice_option_source = key.ice_props['measurement-device-accuracy-salinity_sea-ice_option']['doi']
            source_10_acc_salinity_sea_ice_option = define_direct_source(acc_salinity_sea_ice_option_source, primary_source, secondary_source, tertiary_source)   

        except KeyError:
            acc_salinity_sea_ice_option_source = None
            source_10_acc_salinity_sea_ice_option = 1.0
            pass

        try:
            calc_meth_temperature_sea_ice_source = key.ice_props['calculation-method-temperature_sea-ice']['doi']
            source_10_calc_meth_temperature_sea_ice = define_direct_source(calc_meth_temperature_sea_ice_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            source_10_calc_meth_temperature_sea_ice = 1.0
            calc_meth_temperature_sea_ice_source = None
            pass

        try:
            meas_dev_temperature_sea_ice_source = key.ice_props['measurement-device-temperature_sea-ice']['doi']
            source_10_measurement_device_temperature_sea_ice = define_direct_source(meas_dev_temperature_sea_ice_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            source_10_measurement_device_temperature_sea_ice = 1.0
            meas_dev_temperature_sea_ice_source = None
            pass

        try:
            meas_dev_temperature_sea_ice_option_source = key.ice_props['measurement-device-temperature_sea-ice_option']['doi']
            source_10_measurement_device_temperature_sea_ice_option = define_direct_source(meas_dev_temperature_sea_ice_option_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            source_10_measurement_device_temperature_sea_ice_option = 1.0
            meas_dev_temperature_sea_ice_option_source = None
            pass    
        
        try:
            acc_temperature_sea_ice_source = key.ice_props['measurement-device-accuracy-temperature_sea-ice']['doi']
            source_10_acc_temperature_sea_ice = define_direct_source(acc_temperature_sea_ice_source, primary_source, secondary_source, tertiary_source)   
        except KeyError:
            acc_temperature_sea_ice_source = None
            source_10_acc_temperature_sea_ice = 1.0

        try: 
            water_body_source = key.ice_props['water-body']['doi']
            source_10_water_body= define_direct_source(water_body_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            water_body_source = None
            source_10_water_body = 1.0   

        try: 
            water_body_option_source = key.ice_props['water-body_option']['doi']
            source_10_water_body_option= define_direct_source(water_body_option_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            water_body_option_source = None
            source_10_water_body_option= 1.0

        try: 
            polar_region_source = key.ice_props['polar-region']['doi']
            source_10_polar_region= define_direct_source(polar_region_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            polar_region_source = None
            source_10_polar_region = 1.0   

        try:
            form_sea_ice_source = key.ice_props['form_sea-ice']['doi']
            source_10_form_sea_ice= define_direct_source(form_sea_ice_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            form_sea_ice_source = None  
            source_10_form_sea_ice= 1.0

        try:      
            if key.ice_props['form_sea-ice']['comment'].startswith('derived from SIN_WMO_2014 using sea ice concentration'):
                source_10_form_sea_ice = 1.0  # manual fill
        except (AttributeError, KeyError):
            pass

        try:
            form_sea_ice_option_source = key.ice_props['form_sea-ice_option']['doi']
            source_10_form_sea_ice_option= define_direct_source(form_sea_ice_option_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            form_sea_ice_option_source = None
            source_10_form_sea_ice_option =  1.0

        try:
            development_stage_sea_ice_source = key.ice_props['development-stage_sea-ice']['doi']
            source_10_development_stage_sea_ice = define_direct_source(development_stage_sea_ice_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            development_stage_sea_ice_source = None
            source_10_development_stage_sea_ice = 1.0

        try:
            development_stage_sea_ice_option_source = key.ice_props['development-stage_sea-ice_option']['doi']
            source_10_development_stage_sea_ice_option = define_direct_source(development_stage_sea_ice_option_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            development_stage_sea_ice_option_source = None
            source_10_development_stage_sea_ice_option = 1.0
        
        try:
            thickness_sea_ice_source = key.ice_props['thickness_sea-ice']['doi']
            source_10_thickness_sea_ice = define_direct_source(thickness_sea_ice_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            thickness_sea_ice_source = None
            source_10_thickness_sea_ice = 1.0

        try:
            thickness_sea_ice_comment = key.ice_props['thickness_sea-ice']['comment'] 
            if key.ice_props['thickness_sea-ice']['comment'] is np.nan:
                thickness_sea_ice_comment= None
        except KeyError:
            thickness_sea_ice_comment = None
        if thickness_sea_ice_comment is not None and thickness_sea_ice_comment.startswith('from '):
            source_10_thickness_sea_ice = 1.0 # manual fill with lowest measurement        
        
        try:
            freeboard_sea_ice_source = key.ice_props['freeboard_sea-ice']['doi']
            source_10_freeboard_sea_ice = define_direct_source(freeboard_sea_ice_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            freeboard_sea_ice_source = None
            source_10_freeboard_sea_ice = 1.0
        
        try:
            thickness_snow_source = key.ice_props['thickness_snow']['doi']
            source_10_thickness_snow = define_direct_source(thickness_snow_source, primary_source, secondary_source, tertiary_source)      
        except KeyError:
            thickness_snow_source = None
            source_10_thickness_snow = 1.0
        
        try:
            date_source = key.ice_props['date']['doi']
            source_10_date= define_direct_source(date_source, primary_source, secondary_source, tertiary_source)
        except KeyError:
            date_source = None
            source_10_date = 1.0
        
        try:
            campaign_source = key.ice_props['campaign']['doi']
            source_10_campaign= define_direct_source(campaign_source, primary_source, secondary_source, tertiary_source)   
        except KeyError:
            campaign_source = None
            source_10_campaign = 1.0        
        
        try:
            campaign_option_source = key.ice_props['campaign_option']['doi']
            source_10_campaign_option= define_direct_source(campaign_option_source, primary_source, secondary_source, tertiary_source)   

        except KeyError:
            campaign_option_source = None
            source_10_campaign_option = 1.0                

        try:
            latitude_list = key.ice_props['location']['value']['N']
            longitude_list = key.ice_props['location']['value']['E']
            coordinates_list.append((key.ice_props['location']['value']['N'], key.ice_props['location']['value']['E'])) # for plotting in the map
            coordinate_source = key.ice_props['location']['doi']
            source_10_coordinates = define_direct_source(coordinate_source, primary_source, secondary_source, tertiary_source)
        except KeyError:
            coordinate_source = None
            source_10_coordinates =1.0

        sources_list = [date_source, coordinate_source, water_body_source, water_body_option_source,  campaign_source, campaign_option_source, development_stage_sea_ice_source, development_stage_sea_ice_option_source, form_sea_ice_source, form_sea_ice_option_source,  freeboard_sea_ice_source, thickness_sea_ice_source, thickness_snow_source, temperature_sea_ice_source, meas_dev_temperature_sea_ice_source, meas_dev_temperature_sea_ice_option_source, acc_temperature_sea_ice_source,  salinity_sea_ice_source, meas_dev_salinity_sea_ice_source, meas_dev_temperature_air_source, meas_dev_salinity_sea_ice_option_source, acc_salinity_sea_ice_source, acc_salinity_sea_ice_option_source, temperature_air_source]    
        doi_list = []
        for var in sources_list:
            if var is not None:
                doi_list.append(str(var))
        doi_dict[str(key.name)] = doi_list   
        name_dict[str(key.name)] = str(i)     
        
        campaign_dict[str(key.name)] = key.ice_props['campaign']['value']
        source_doi.extend(doi_list)
        source_core_data=pd.DataFrame(np.array([(key.name, source_10_date, source_10_coordinates, source_10_campaign, source_10_polar_region,source_10_water_body, source_10_form_sea_ice, source_10_development_stage_sea_ice, source_10_salinity_sea_ice, source_10_measurement_device_salinity_sea_ice , source_10_acc_salinity_sea_ice, source_10_temperature_air,  source_10_measurement_device_temperature_air , source_10_acc_temperature_air, source_10_temperature_sea_ice, source_10_calc_meth_temperature_sea_ice, source_10_measurement_device_temperature_sea_ice , source_10_acc_temperature_sea_ice, source_10_freeboard_sea_ice,  source_10_thickness_sea_ice,  source_10_thickness_snow)]), columns = ['id' ,            'date','coordinates', 'campaign', 'polar-region',             'water-body',     'form_sea-ice',          'development-stage_sea-ice',  'salinity_sea-ice', 'measurement-device-salinity_sea-ice',  'measurement-device-accuracy-salinity_sea-ice',        'temperature_air',           'measurement-device-temperature_air', 'measurement-device-accuracy-temperature_air', 'temperature_sea-ice', 'calculation-method-temperature_sea-ice', 'measurement-device-temperature_sea-ice',     'measurement-device-accuracy-temperature_sea-ice', 'freeboard_sea-ice', 'thickness_sea-ice', 'thickness_snow' ], index = [indexx])      
        source_sea_ice_per_core= pd.concat([source_sea_ice_per_core, source_core_data])
        indexx= indexx+1
    return source_sea_ice_per_core, doi_dict, name_dict, campaign_dict
    

    
def categorize_sources(doi_dict, campaign_dict, name_dict, primary_source, secondary_source, tertiary_source):
    '''
    Categorizes the sources into 'A', 'B', or 'C' based on their type.
    '''
    # Create a nested dictionary to store the categorized values
    dict_2 = {}

    # Iterate through the keys and values in doi_dict
    for key, values in doi_dict.items():
        # Create a dictionary to store the categorized values for the current key
        categorized_values = {'A': [], 'B': [], 'C': []}

        # Iterate through each value in the current key's list
        for value in values:
            # Categorize the value into 'A', 'B', or 'C' and add it to the dictionary
            if value in primary_source:
                categorized_values['A'].append(value)
            elif value in secondary_source:
                categorized_values['B'].append(value)
            elif value in tertiary_source:
                categorized_values['C'].append(value)
        # Check if 'C' list is empty, and if not, add the categorized values to dict_2 with the current key
        if categorized_values['C']:
            dict_2[key] = categorized_values
        else:
            # If 'C' is empty, exclude it from the resulting dictionary
            categorized_values.pop('C', None)
            if categorized_values:
                dict_2[key] = categorized_values

        # Add the 'campaign' information to the dictionary
        dict_2[key]['campaign'] = [campaign_dict[key]]
        dict_2[key]['core'] = [name_dict[key]]

    # Sort the dictionary based on 'campaign' and 'B'
    # print(dict_2[key]['core'])
    sorted_dict = dict(sorted(dict_2.items(), key=lambda x: ( x[1]['campaign'] ,max(x[1]['B']), max(x[1]['A']))))
    return sorted_dict

def sort_dicts (sorted_dict):
    # Initialize lists to store A, B, and C values
    primary_source_sorted = []
    secondary_source_sorted = []
    tertiary_source_sorted = []
    core_name_sorted = []
    campaign_y_label_sorted = []
    # Iterate through the sorted dictionary
    for values in sorted_dict.values():
        # Extend the lists with the values from the current key
        primary_source_sorted.extend(values.get('A', []))
        secondary_source_sorted.extend(values.get('B', []))
        tertiary_source_sorted.extend(values.get('C', []))
        campaign_y_label_sorted.extend(values.get('campaign', []))
        core_name_sorted.extend(values.get('core', []))
    primary_source_sorted_short = list(dict.fromkeys(primary_source_sorted))
    secondary_source_sorted_short = list(dict.fromkeys(secondary_source_sorted))
    tertiary_source_sorted_short = list(dict.fromkeys(tertiary_source_sorted))
    campaign_y_label_sorted_short = list(dict.fromkeys(campaign_y_label_sorted))
    core_name_sorted_short = list(dict.fromkeys(core_name_sorted))
    return primary_source_sorted_short, secondary_source_sorted_short, tertiary_source_sorted_short, campaign_y_label_sorted_short, core_name_sorted_short, primary_source_sorted, secondary_source_sorted, tertiary_source_sorted, campaign_y_label_sorted, core_name_sorted

def mapping_index(primary_source_sorted_short, secondary_source_sorted_short, tertiary_source_sorted_short, core_name_sorted_short, primary_source_sorted, secondary_source_sorted, tertiary_source_sorted, core_name_sorted):
        
    index_mapping_A = {value: (primary_source_sorted_short.index(value) / (len(primary_source_sorted_short) - 1) *286 )  +0.5 for value in primary_source_sorted}
    index_mapping_B = {value: (secondary_source_sorted_short.index(value) /(len(secondary_source_sorted_short) - 1) * 286 ) +0.5 for value in secondary_source_sorted}
    index_mapping_C = {value: (tertiary_source_sorted_short.index(value) / (len(tertiary_source_sorted_short) -1 ) *286 )+0.5 for value in tertiary_source_sorted}
    index_mapping_core = {value: (core_name_sorted_short.index(value) / (len(core_name_sorted_short)-1) *286) +0.5 for value in core_name_sorted}
    return index_mapping_A, index_mapping_B, index_mapping_C, index_mapping_core

def apply_mapping(sorted_dict, index_mapping_A, index_mapping_B, index_mapping_C, index_mapping_core):
    # Apply the mappings to the nested dictionary
    for key1, subdict in sorted_dict.items():
        for subkey, values in subdict.items():
            # Check which mapping to use based on the current subkey
            if values[0] in index_mapping_A:
                sorted_dict[key1][subkey] = [index_mapping_A[value] for value in values]
            elif values[0] in index_mapping_B:
                sorted_dict[key1][subkey] = [index_mapping_B[value] for value in values]
            elif values[0] in index_mapping_C:
                sorted_dict[key1][subkey] = [index_mapping_C[value] for value in values]
            elif values[0] in index_mapping_core:
                sorted_dict[key1][subkey] = [index_mapping_core[value] for value in values]
            # ... repeat for other mappings
    return sorted_dict

def order_plot(source_sea_ice_per_core, sorted_dict):
    order_name = list(sorted_dict.keys())
    # Create a categorical column with the desired order
    source_sea_ice_per_core['order_category'] = pd.Categorical(source_sea_ice_per_core['id'], categories=order_name, ordered=True)
    source_sea_ice_per_core.sort_values('order_category', inplace=True)

    # Drop the temporary categorical column
    source_sea_ice_per_core.drop(columns=['order_category'], inplace=True)

    # Reset the index if needed
    source_sea_ice_per_core.reset_index(drop=True, inplace=True)
    return source_sea_ice_per_core

def data2source_traceability_plot(source_sea_ice_per_core, sorted_dict, index_mapping_A, index_mapping_B, index_mapping_C, index_mapping_core, core_name_sorted_short,primary_source_sorted_short,secondary_source_sorted_short,tertiary_source_sorted_short, output_dir = None):
    if output_dir is None:
        output_dir = os.path.join(parent_dir, 'plots', 'data2source_traceability')  # Default output directory within the package
    else:
        output_dir = os.path.join(output_dir,'data2source_traceability')
    # Ensure output directory exists
    os.makedirs(output_dir, exist_ok=True)
    # Set the figure size to DIN A4 size in inches (8.27 x 11.69)
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=( 9,11.69), sharey=True)
    sns.set(font_scale=0.8)  # Adjust the scale as needed
    ##  drop core column and make everything numeric
    source_sea_ice_per_core_dropcore = source_sea_ice_per_core.drop(columns=['id'])
    source_sea_ice_per_core_dropcore = source_sea_ice_per_core_dropcore.apply(pd.to_numeric, errors='coerce')
    # Add the first subplot on the left
    YAML_file_list, data_list = load_yaml_files()
    ax1 = sns.heatmap(source_sea_ice_per_core_dropcore , ax = ax1, cmap='Dark2', linewidth=0.005, cbar= False, linecolor='grey', yticklabels=True)
    ax1.set(ylabel='Sea ice cores')
    ## Get the unique colors from the heatmap
    colors = np.unique(ax1.get_children()[0].get_facecolor(), axis=0)
  #  colors = colors[colors[:, 1].argsort()]
    # Y-axis Original range of values
    original_range_y = range(0, 288, 20)

    # Add 0.5 to each value using list comprehension
    new_range_y = [value + 0.5 for value in original_range_y]

    ax1.set_yticks(new_range_y[::-1])
    ax1.set_yticklabels(original_range_y[::-1], fontsize=10)
    ax1.annotate('(a)',
                xy=(9.5, 292.5),
                horizontalalignment='left', verticalalignment='top',
                fontsize=10, color='black')
    # X-axis Original range of values
    original_range_x = range(len(source_sea_ice_per_core_dropcore.columns))

    # Add 0.5 to each value using list comprehension
    new_range_x = [value + 0.5 for value in original_range_x]
    
    ax1.set_xticks(new_range_x)
    ax1.set_xticklabels(source_sea_ice_per_core_dropcore.columns, rotation=90, fontsize=10)
    # Make spines visible
    for _, spine in ax1.spines.items():
        spine.set_visible(True)

    # Remove the box around the plot
    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax1.spines['bottom'].set_visible(False)
    ax1.spines['left'].set_visible(False)
    ax1.set_facecolor('white')    
    url_coord_x = []
    url_coord_y = []
    url_list = []
    # Add the second subplot on the right
    # Define categories including the fourth category (keys)
    categories = ['Cores #287', 'Primary sources #107', 'Secondary sources #23', 'Tertiary sources #8']

    # Iterate through each key in the dictionary
    for key, values in sorted_dict.items():
        # Create a list to store coordinates for each line

        key_value = sorted_dict[key]['core'][0]
        # Iterate through each value in category 'A'
        for value_A in values['A']:
            lines = []
            # Add the key as the first value in the lines
            lines.append((categories.index('Cores #287'), key_value))
            lines.extend([(categories.index('Primary sources #107'), value_A)])
                    # Convert lines to numpy array for plotting
            lines = np.array(lines)

            # Plot the lines for the key without markers, adjust linewidth
            ax2.plot(lines[:, 0], lines[:, 1],  '-', color='grey', linewidth=0.3, markevery=[-1], markerfacecolor=colors[0], markeredgecolor=colors[1], markersize = 7, alpha = 0.5)
            # Plot a marker on the left side (excluding the last point)
            ax2.plot(lines[:-1, 0], lines[:-1, 1], marker='o', linestyle='None', color='grey', markersize=1)
            url_coord_x.append(categories.index('Primary sources #107'))
            url_coord_y.append(value_A)

            # Set URLs for each marker
            url_list.append (str(get_key_from_value(index_mapping_A, value_A)))
            for value_B in values['B']:
                lines = []
                # Add the key as the first value in the lines
                lines.append((categories.index('Primary sources #107'), value_A))
                lines.extend([(categories.index('Secondary sources #23'), value_B)])
                        # Convert lines to numpy array for plotting
                lines = np.array(lines)

                # Plot the lines for the key without markers, adjust linewidth
                ax2.plot(lines[:, 0], lines[:, 1], '-', color='grey', linewidth=0.3, markevery=[-1], markerfacecolor=colors[2], markeredgecolor=colors[2], markersize = 12, alpha = 0.5)  

                # If 'C' is present, connect each value in 'A' to all values in 'B' and 'C'
                if 'C' in values:
                    for value_C in values['C']:
                        lines = []
                        # Add the key as the first value in the lines
                        lines.append((categories.index('Secondary sources #23'), value_B))
                        lines.extend([(categories.index('Tertiary sources #8'), value_C)])
                                # Convert lines to numpy array for plotting
                        lines = np.array(lines)

                        # Plot the lines for the key without markers, adjust linewidth
                        ax2.plot(lines[:, 0], lines[:, 1], '-', color='grey', linewidth=0.3, markevery=[-1], markerfacecolor=colors[3], markeredgecolor=colors[3], markersize = 12, alpha = 0.5) 
    colors_list_core = ['grey'] * len(core_name_sorted_short) 
    colors_list =  [colors[0]] * len(primary_source_sorted_short) + [colors[2]] * len(secondary_source_sorted_short) + [colors[3]] * len(tertiary_source_sorted_short)
    coord_x = [1] * len(primary_source_sorted_short) +  [2] * len(secondary_source_sorted_short)+ [3] * len(tertiary_source_sorted_short)                                            
    coord_xx =[0] * len(core_name_sorted_short)      
    coord_y =  [index_mapping_A[k] for k in primary_source_sorted_short]  + [index_mapping_B[k] for k in secondary_source_sorted_short] + [index_mapping_C[k] for k in tertiary_source_sorted_short]   
    coord_yy =  [index_mapping_core[k] for k in core_name_sorted_short]  
    sizes_list = [8] * len(primary_source_sorted_short) + [14] * len(secondary_source_sorted_short) + [14] * len(tertiary_source_sorted_short)
    s1= ax2.scatter(coord_x, coord_y, color = colors_list, marker = 'o', s= sizes_list)
    url_list =  primary_source_sorted_short + secondary_source_sorted_short + tertiary_source_sorted_short
    s1.set_urls(url_list)

    s2= ax2.scatter(coord_xx, coord_yy, color = colors_list_core, marker = 'o', s =1.5)
    core_name_sorted_short_file = ["https://git.rwth-aachen.de/mbd/pyresice/-/blob/main/src/pyresice/RESICE_extendable_database/yaml_db/earth/" + item for item in YAML_file_list] # add gitLab link to respectvie yaml file
    s2.set_urls(core_name_sorted_short_file) 
    # Set labels and remove key names
    ax2.set_xticks(range(len(categories)))

    ax2.set_xticklabels(categories, rotation=90, fontsize=10)
    plt.ylim([-5,292])
    plt.xlim([-.05,3.2])
    #ax2.tick_params(axis='y', color = 'white')
    #ax2.legend().set_visible(False)  # Remove legend
    
    ax2.annotate('(b)',
            xy=(1.5, 292.5),
            horizontalalignment='left', verticalalignment='top',
            fontsize=10, color='black')
    # Remove the box around the plot
    ax2.spines['top'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax2.set_facecolor('white')
    plt.grid(False)

    # Create legend with color labels
    legend_labels = ['Primary sources', 'Secondary sources', 'Tertiary sources', 'Not available'] # 
    # Create rectangular legend handles
    legend_handles = [plt.Rectangle((0,0),1,1, color=color, ec=color, lw=3) for color in [colors[0], colors[2], colors[3], colors[1]]]

    # Add legend below the plot
    fig.legend(legend_handles, legend_labels,  loc='upper center', bbox_to_anchor=(0.85, 0.095), ncol=1, frameon=True, edgecolor='black', facecolor='white', fontsize=10)

    # Adjust layout for better spacing

    # Show the plot
    fig.tight_layout()

    output_path_svg = os.path.join(output_dir, 'interactive_data2source_traceability.svg')
    output_path_pdf = os.path.join(output_dir, 'data2source_traceability.pdf')
    output_path_png = os.path.join(output_dir, 'data2source_traceability.png')
    fig.savefig(output_path_svg,  dpi = 500, transparent=True) # bbox_inches='tight',
    fig.savefig(output_path_pdf, dpi = 500, transparent=True) #bbox_inches='tight', 
    fig.savefig(output_path_png,  dpi = 500, transparent=True) #bbox_inches='tight',
    return fig

def create_data2source_traceability_plot():
    YAML_file_list, data_list = load_yaml_files()
    primary_source, secondary_source, tertiary_source = load_source_group_definition()
    source_sea_ice_per_core, doi_dict, name_dict, campaign_dict = create_tabular_db_source_categories(YAML_file_list, data_list, primary_source, secondary_source, tertiary_source)
    sorted_dict =categorize_sources(doi_dict, campaign_dict, name_dict, primary_source, secondary_source, tertiary_source)
    primary_source_sorted_short, secondary_source_sorted_short, tertiary_source_sorted_short, campaign_y_label_sorted_short, core_name_sorted_short, primary_source_sorted, secondary_source_sorted, tertiary_source_sorted, campaign_y_label_sorted, core_name_sorted = sort_dicts(sorted_dict)
    index_mapping_A, index_mapping_B, index_mapping_C, index_mapping_core = mapping_index(primary_source_sorted_short, secondary_source_sorted_short, tertiary_source_sorted_short, core_name_sorted_short, primary_source_sorted, secondary_source_sorted, tertiary_source_sorted, core_name_sorted)
    sorted_dict = apply_mapping(sorted_dict, index_mapping_A, index_mapping_B, index_mapping_C, index_mapping_core)
    source_sea_ice_per_core = order_plot(source_sea_ice_per_core, sorted_dict)
    data2source_traceability_plot(source_sea_ice_per_core, sorted_dict, index_mapping_A, index_mapping_B, index_mapping_C, index_mapping_core, core_name_sorted_short, primary_source_sorted_short, secondary_source_sorted_short, tertiary_source_sorted_short)
    return
