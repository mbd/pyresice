from pyresice.read.read_funcs import load_yaml_files
import pandas as pd
import numpy as np



def reorder_enriched_tabular_RESICE(df):
    # Reorder the columns of the dataframe
    df = df.reindex(columns=['id', 'date', 'date_source', 'date_doi',
        'latitude', 'longitude', 'coordinates_source', 'coordinates_doi', 
        'polar_region', 'polar_region_source','polar_region_doi',
        'water_body', 'water_body_source', 'water_body_comment',
        'sea_SeaVoX', 'ocean_SeaVoX', 
        'campaign','campaign_comment', 'campaign_source', 'campaign_doi',
        'form_sea_ice', 'form_sea_ice_comment', 'form_sea_ice_source', 'form_sea_ice_doi',
        'development_stage_sea_ice','development_stage_sea_ice_comment', 'development_stage_sea_ice_source', 'development_stage_sea_ice_doi', 
        'development_stage_SIN_level_1_sea_ice', 'development_stage_SIN_level_2_sea_ice',
        'INFO_SIN', 'INFO_residual_ice',
        'thickness_sea_ice_comment','thickness_sea_ice_source', 'thickness_sea_ice_doi', 
        'freeboard_sea_ice_comment','freeboard_sea_ice_source', 'freeboard_sea_ice_doi', 
        'thickness_snow_comment', 'thickness_snow_source','thickness_snow_doi',
        'temperature_air_comment','temperature_air_adjusted', 'temperature_air_source', 'temperature_air_doi',
        'measurement_device_temperature_air', 'measurement_device_temperature_air_comment', 'measurement_device_temperature_air_source', 'measurement_device_temperature_air_doi', 
        'measurement_device_accuracy_temperature_air [K]', 'measurement_device_accuracy_temperature_air_comment', 
        'measurement_device_accuracy_temperature_air_source', 'measurement_device_accuracy_temperature_air_doi',
        'temperature_sea_ice_comment', 'temperature_sea_ice_adjusted', 'temperature_sea_ice_source', 'temperature_sea_ice_doi', 
        'measurement_device_temperature_sea_ice', 'measurement_device_temperature_sea_ice_comment', 'measurement_device_temperature_sea_ice_source', 'measurement_device_temperature_sea_ice_doi',
        'calculation_method_temperature_sea_ice','calculation_method_temperature_sea_ice_comment' ,'calculation_method_temperature_sea_ice_source', 'calculation_method_temperature_sea_ice_doi',
        'measurement_device_accuracy_temperature_sea_ice [K]','measurement_device_accuracy_temperature_sea_ice_comment', 'measurement_device_accuracy_temperature_sea_ice_source', 'measurement_device_accuracy_temperature_sea_ice_doi', 
        'mean_temperature_sea_ice [K]',
        'mean_distance_measurements_temperature_sea_ice [m]',
        'salinity_sea_ice_comment', 'salinity_sea_ice_adjusted', 'salinity_sea_ice_source', 'salinity_sea_ice_doi', 
        'measurement_device_salinity_sea_ice', 'measurement_device_salinity_sea_ice_comment', 'measurement_device_salinity_sea_ice_source','measurement_device_salinity_sea_ice_doi',
        'measurement_device_accuracy_salinity_sea_ice [ppt]', 'measurement_device_accuracy_salinity_sea_ice_comment', 'measurement_device_accuracy_salinity_sea_ice_source', 'measurement_device_accuracy_salinity_sea_ice_doi',
        'mean_salinity_sea_ice [ppt]',
        'mean_distance_measurements_salinity_sea_ice [m]',      
        'thickness_sea_ice [m]', 'freeboard_sea_ice [m]', 'thickness_snow [m]',
        'temperature_air [K]',
        'depth_temperature_sea_ice [m]', 'temperature_sea_ice [K]',
        'depth_salinity_sea_ice [m]', 'salinity_sea_ice [ppt]'
        ]) 
    return df


def dataframe_onedepth(df):
    '''
    Function to merge temperature and salinity dataframes on depth
    '''
    df_T= df.loc[:, : 'temperature_sea_ice [K]']
    df_T = df_T.dropna(subset=['temperature_sea_ice [K]']) # drop all a 
    df_T =df_T.rename(columns={"depth_temperature_sea_ice [m]": "depth [m]"}) # overwrite the column name
    df_S= df.drop(columns=['temperature_sea_ice [K]', 'depth_temperature_sea_ice [m]'], axis = 1)

    df_S= df_S.dropna(subset=['salinity_sea_ice [ppt]'])
    df_S= df_S.rename(columns={"depth_salinity_sea_ice [m]": "depth [m]"}) # overwrite the column name
    df_onedepth = pd.merge_ordered(df_T, df_S) # merge depth column
    df_onedepth.index.name='Index'
    return df_onedepth