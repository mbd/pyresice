import matplotlib.pyplot as plt
import seaborn as sns
import ipywidgets as widgets
import numpy as np

def overview_10(df):
    # Create a boolean DataFrame where True indicates non-empty and False indicates empty
    non_empty = df.notnull()

    plt.figure(figsize=(8,10))  # Optional: Adjust the size of your plot
    sns.heatmap(non_empty.iloc[:, :], cbar=False)
    plt.show()
    return

def plot_temp_salinity_combined(df, index):
    plt.clf()
    ids = df['id'].unique()
    data = df[df['id'] == ids[index]]

    plt.figure(figsize=(8, 4))  # Wider figure to accommodate two subplots

    # Plot for Temperature
    plt.subplot(1, 2, 1)  # 1 row, 2 columns, subplot 1
    plt.plot(data['temperature_sea_ice [K]'], data['depth [m]'], 'r.')
    plt.gca().invert_yaxis()  # Depth increases downwards
    plt.xlabel('Temperature [K]')
    plt.ylabel('Depth [m]')
    plt.title(f'Temperature: Depth for ID {ids[index]}')

    # Plot for Salinity
    plt.subplot(1, 2, 2)  # 1 row, 2 columns, subplot 2
    plt.plot(data['salinity_sea_ice [ppt]'], data['depth [m]'], 'b.')
    plt.gca().invert_yaxis()
    plt.xlabel('Salinity [ppt]')
    plt.ylabel('Depth [m]')
    plt.title(f'Salinity: Depth for ID {ids[index]}')

    plt.tight_layout()  # Adjust layout to not overlap
    plt.show()

def temp_sal_profile_interactive(df):
    # Get the unique ids and set up the slider
    ids_id = df['id'].unique()
    ids = np.arange(0, len(ids_id))
    id_slider = widgets.IntSlider(min=ids.min(), max=ids.max(), step=1, value=ids.min())

    # Create the interactive plot
    interactive_plot = widgets.interact(plot_temp_salinity_combined, df=widgets.fixed(df), index=id_slider)
