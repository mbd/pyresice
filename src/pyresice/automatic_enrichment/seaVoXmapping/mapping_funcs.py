import geopandas as gpd
import pickle
from shapely.geometry import Point
import pandas as pd

def coordinates_dataframe(df):
    '''
    Function to get a dataframe of all unique coordinates
    '''
    # Get the coordinates from the dataframe
    coordinates = set(df[['latitude', 'longitude']].apply(tuple, axis=1))

    # remove all duplicates of latitude and longitude combinations
    coordinates = coordinates.drop_duplicates(subset=['latitude', 'longitude'], keep='first')
    coordinates.reset_index(drop=True, inplace=True)
    coord_in_df = list(zip(coordinates['latitude'], coordinates['longitude']))
    return coord_in_df

def coordinates_mapping():
    Sea_SeaVoX_dict, Ocean_SeaVoX_dict = load_seavox_mapping_file()
    ocean_seavox_keys = list(Ocean_SeaVoX_dict.keys())
    sea_seavox_keys = list(Sea_SeaVoX_dict.keys())
    coord_in_mapping_list = ocean_seavox_keys + sea_seavox_keys    
    return coord_in_mapping_list

def check_for_new_coordinates(df):
    '''
    Function to check if there are new coordinates in the dataframe that are not in the Sea_SeaVoX_dict and Ocean_SeaVoX_dict
    '''
    coord_in_mapping_list = coordinates_mapping()
    coord_in_df = coordinates_dataframe(df)
    # Make a list from all coordinates already mapped to ocean and sea

    # Check for new coordinates
    new_coords = []
    for item in coord_in_df:
        if item not in coord_in_mapping_list:
            new_coords.append(item)
    # Create a dataframe from the new_coords
    df_new_coords = pd.DataFrame(new_coords, columns=['latitude', 'longitude'])
    if df_new_coords.empty:
        print("df_new_coords is empty: no new coordiante was added")
    return df_new_coords

def map_new_coordinates(df_new_coords):
    '''
    Function to map new coordinates to ocean and sea from SeaVox CV
    '''
    # Apply the get_sub_region_name() and get_ocean_region function to each row in the dataframe
    df_new_coords['Ocean_SeaVox'] = df_new_coords.apply(lambda row: get_ocean_name(row['latitude'], row['longitude']), axis=1)
    df_new_coords['Sea_SeaVox'] = df_new_coords.apply(lambda row: get_sub_region_name(row['latitude'], row['longitude'], ), axis=1)
    
    # create a mapping as dictionary to map lat long combinations to sea
    new_Sea_SeaVoX_dict = df_new_coords.set_index(['latitude', 'longitude']).to_dict()['Sea_SeaVox']

    # create a mapping as dictionary to map lat long combinations to ocean
    new_Ocean_SeaVoX_dict = df_new_coords.set_index(['latitude', 'longitude']).to_dict()['Ocean_SeaVox']

    return new_Sea_SeaVoX_dict, new_Ocean_SeaVoX_dict

def extend_mappings(df_new_coords):
    '''
    Function to map new coordinates to ocean and sea
    '''
    new_Sea_SeaVoX_dict, new_Ocean_SeaVoX_dict = map_new_coordinates(df_new_coords)
    Sea_SeaVoX_dict, Ocean_SeaVoX_dict = load_seavox_mapping_file()
    Ocean_SeaVoX_dict.update(new_Ocean_SeaVoX_dict)
    Sea_SeaVoX_dict.update(new_Sea_SeaVoX_dict)

    with open('Sea_SeaVoX_dict.pickle', 'wb') as handle:
        pickle.dump(Sea_SeaVoX_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)

    # store Sea_SeaVox_dict in a file
    with open('Ocean_SeaVoX_dict.pickle', 'wb') as handle:
        pickle.dump(Ocean_SeaVoX_dict, handle, protocol=pickle.HIGHEST_PROTOCOL)
    return

def load_seavox_mapping_file():
    # load Sea_SeaVox_dict from file
    with open('/Sea_SeaVoX_dict.pickle', 'rb') as handle:
        Sea_SeaVoX_dict = pickle.load(handle)

    # load Ocean_SeaVox_dict from file
    with open('/Ocean_SeaVoX_dict.pickle', 'rb') as handle:
        Ocean_SeaVoX_dict = pickle.load(handle)

    return Sea_SeaVoX_dict, Ocean_SeaVoX_dict


def load_shapefile(shapefile_path="SeaVoX_polygons/"):
    """
    Load a shapefile
    """
    return gpd.read_file(shapefile_path)


def get_ocean_name(
    latitude, longitude, shapefile_path="SeaVoX_polygons/"
):
    '''
    Function to get the ocean name for a given set of coordinates
    '''
    gdf = load_shapefile(shapefile_path)
    point = Point(longitude, latitude)
    # Check which region contains the point
    for index, row in gdf.iterrows():
        if row["geometry"].contains(point):
            print(latitude, longitude, row["REGION"])
            return row["REGION"]
            # Return None if no region contains the point
    return None 


def get_sub_region_name(
    latitude, longitude, shapefile_path="SeaVoX_polygons/"
):
    '''
    Function to get the region name for a given set of coordinates
    '''
    gdf = load_shapefile(shapefile_path)
    point = Point(longitude, latitude)

    # Check which region contains the point
    for index, row in gdf.iterrows():
        if row["geometry"].contains(point):
            return row["SUB_REGION"]
            # Return None if no region contains the point
    return None

